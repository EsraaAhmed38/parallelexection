package Tests;

import Config.ProperitiesFile;
import Pages.LoginSeamlesPage;
import io.appium.java_client.android.AndroidDriver;
import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class LoginSeamles2 {


    public static WebDriverWait wait = null;
    public static JavascriptExecutor js = null;


    @Test
    public void LoginWithRedNumber() throws Exception {
        ProperitiesFile.getProperties();
        BasicConfigurator.configure();//Logger intaliazation
        File classpathRoot = new File(System.getProperty("user.dir"));
        System.out.println(classpathRoot);
        File appDir = new File(classpathRoot, "/Apps/Vodafone/");
        File app = new File(appDir, "regressionCycle2.apk");
        // Created object of DesiredCapabilities class.
        // System.out.println(app);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("udid", "emulator-5556");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("noReset", "false");
        // capabilities.setCapability("appWaitActivity", "*");
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("automationName", "UiAutomator2");
        capabilities.setCapability("systemPort", "8203");
        //capabilities.setCapability("uiautomator2ServerLaunchTimeout", 20000);
        capabilities.setCapability("appPackage", "com.emeint.android.myservices");
        capabilities.setCapability("appActivity", "vodafone.vis.engezly.ui.screens.splash.SplashRevampActivity");
        AndroidDriver driver = new AndroidDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 15000);
        js = (JavascriptExecutor) driver;
        driver.findElement(By.id("com.emeint.android.myservices:id/skipBtn")).click();
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText")
        ).sendKeys("01000772017");
         driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText").sendKeys("Test@1234");
         driver.findElement(By.id("com.emeint.android.myservices:id/btnLogin")).click();
        WebElement offerTab = driver.findElementByAccessibilityId("Offers");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(offerTab));
        offerTab.click();
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Customize My Offer"+"\").instance(0))").click();
        Thread.sleep(8000);
        driver.findElementByAccessibilityId("offersTab-btnCustomize your offer now and pick your favorite offer").click();

    }


    @Test
    public void LoginWithRedNumber2() throws Exception {
        ProperitiesFile.getProperties();
        BasicConfigurator.configure();//Logger intaliazation
        File classpathRoot = new File(System.getProperty("user.dir"));
        System.out.println(classpathRoot);
        File appDir = new File(classpathRoot, "/Apps/Vodafone/");
        File app = new File(appDir, "regressionCycle2.apk");
        // Created object of DesiredCapabilities class.
        // System.out.println(app);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("noReset", "false");
        // capabilities.setCapability("appWaitActivity", "*");
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("automationName", "UiAutomator2");
        capabilities.setCapability("systemPort", "8201");
        //capabilities.setCapability("uiautomator2ServerLaunchTimeout", 20000);
        capabilities.setCapability("appPackage", "com.emeint.android.myservices");
        capabilities.setCapability("appActivity", "vodafone.vis.engezly.ui.screens.splash.SplashRevampActivity");
        AndroidDriver driver = new AndroidDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 15000);
        js = (JavascriptExecutor) driver;

        driver.findElement(By.id("com.emeint.android.myservices:id/skipBtn")).click();

        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText")
        ).sendKeys("010909699899");

        driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText").sendKeys("Test@123");
        driver.findElement(By.id("com.emeint.android.myservices:id/btnLogin")).click();

        WebElement managebtn = driver.findElement(By.id("com.emeint.android.myservices:id/btnAction"));
        new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(managebtn));
        driver.findElement(By.id("com.emeint.android.myservices:id/btnAction")).click();
        Thread.sleep(6000);
         driver.findElementByAccessibilityId("mi_Borrow_MI_Quota").click();
         driver.findElement(By.id("com.emeint.android.myservices:id/etAmount")).sendKeys("1000");
         driver.findElement(By.id("com.emeint.android.myservices:id/btnRequest")).click();


    }


}
