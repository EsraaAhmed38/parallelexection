package Tests;

import Config.ProperitiesFile;
import Utilities.ThreadLocalDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

//import org.omg.PortableInterceptor.USER_EXCEPTION;

public class Testbase extends ProperitiesFile {

    // static String driverPath ;
    public static AndroidDriver driver = null;
    public static WebDriverWait wait = null;
    public static String projectpath = null;
    public static String appPackage = null;
    public static String appActivity = null;
    public static String screenshotPath = null;
    public static String screenshotPath1 = null;
    public static String screenshotPath2 = null;
    public static String screenshotPath3 = null;
    public static String screenshotPath4 = null;
    public static String screenshotPath5 = null;
    public static String screenshotPath6 = null;
    public static String screenshotPath7 = null;
    public static String screenshotPath8 = null;
    public static String screenshotPath9 = null;
    public static String screenshotPath10 = null;
    public static String screenshotPath11 = null;
    public static String screenshotPath12 = null;
    public static String screenshotPath13 = null;
    public static String screenshotPath14 = null;
    public static String screenshotPath15 = null;
    public static String screenshotPath16 = null;
    public static int ReportID = 0;
    public static JavascriptExecutor js = null;
    private final ThreadLocalDriver threadLocalDriver = new ThreadLocalDriver();
    public AndroidDriver driver1 = null;

    @BeforeMethod(alwaysRun = true)
    @Parameters({"udid", "platformVersion"})
    public void setup(String udid, String platformVersion) throws IOException {
        ProperitiesFile.getProperties();
        BasicConfigurator.configure();//Logger intaliazation
        File classpathRoot = new File(System.getProperty("user.dir"));
        System.out.println(classpathRoot);
        File appDir = new File(classpathRoot, "/Apps/Vodafone/");
        File app = new File(appDir, "regressionCycle2.apk");
        // Created object of DesiredCapabilities class.
        // System.out.println(app);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformVersion", platformVersion);
        capabilities.setCapability("udid", udid);
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("noReset", "false");
        // capabilities.setCapability("appWaitActivity", "*");
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("automationName", "UiAutomator2");
        //capabilities.setCapability("systemPort", systemPort);
        //capabilities.setCapability("uiautomator2ServerLaunchTimeout", 20000);
        capabilities.setCapability("appPackage", "com.emeint.android.myservices");
        capabilities.setCapability("appActivity", "vodafone.vis.engezly.ui.screens.splash.SplashRevampActivity");


        threadLocalDriver.setTLDriver(new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4444/wd/hub"), capabilities));
        driver1 = threadLocalDriver.getTLDriver();
        driver1.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver1, 15000);
        js = (JavascriptExecutor) driver1;

        }


    }


//	 @AfterTest
//	 public void teardown() {
//
//
//		 driver1.closeApp();
//
//	 }

