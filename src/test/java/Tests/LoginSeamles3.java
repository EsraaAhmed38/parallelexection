package Tests;

import Pages.LoginSeamlesPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class LoginSeamles3 extends Testbase {

    @AndroidFindAll({
            @AndroidBy(uiAutomator = "new UiSelector().text(\"Vodafone Minutes\")"),
            @AndroidBy(uiAutomator = "new UiSelector().text(\"دقائق لارقام فودافون\")"),
    })
    private MobileElement minutesGift;

    public MobileElement getMinutesGift() {
        return minutesGift;
    }

    @Test
    public void LoginWithRedNumber() throws Exception {


        driver1.findElement(By.id("com.emeint.android.myservices:id/skipBtn")).click();

        driver1.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText")
        ).sendKeys("010909699899");
        // driver1.findElement(By.id("com.emeint.android.myservices:id/btnLogin")).click();

        driver1.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText").sendKeys("Test@123");

        driver1.findElement(By.id("com.emeint.android.myservices:id/btnLogin")).click();


        WebElement onBoarding = driver1.findElement(By.id("com.emeint.android.myservices:id/btnContinue"));

        new WebDriverWait(driver1, 10).until(ExpectedConditions.visibilityOf(onBoarding));
        onBoarding.click();

        WebElement managebtn = driver1.findElement(By.id("com.emeint.android.myservices:id/btnAction"));

        new WebDriverWait(driver1, 10).until(ExpectedConditions.visibilityOf(managebtn));
        managebtn.click();

        driver1.findElement(By.id("com.emeint.android.myservices:id/btnActio")).click();
        driver1.findElement(By.id("com.emeint.android.myservices:id/laterTv")).click();
        driver1.findElement(By.id("com.emeint.android.myservices:id/cvAfterBundleOption")).click();


    }
}
