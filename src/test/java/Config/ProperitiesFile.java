package Config;

import Tests.Testbase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

public class ProperitiesFile {

    protected static Properties prop = new Properties();

    static String projectpath = System.getProperty("user.dir");
    private static String propertyFilePath = projectpath + "//src//test//java//Config//config.properties";
    public WebDriver driver;
    protected WebDriverWait wait;

    public static void getProperties() {

        try {

            // Read configuration.properties file
            // InputStream input = new FileInputStream(propertyFilePath);
            /// ---- Watchlist-------
            prop.load(new FileInputStream(propertyFilePath));
            Testbase.projectpath = getprojectPath();
            Testbase.appPackage = getappPackage();
            // setProperties();
            Testbase.appActivity = getappActivity();

            System.out.println(prop.getProperty("projectPath"));
        } catch (Exception exp) {
            System.out.println(exp.getMessage());
            System.out.println(exp.getCause());

        }

    }

    public static String getprojectPath() {
        return prop.getProperty("projectPath");
    }

    public static String getappPackage() {
        return prop.getProperty("appPackage");
    }

    public static String getappActivity() {
        return prop.getProperty("appActivity");
    }


    /// to change property in properties file//
    public static void setProperties() {

        try {

            OutputStream output = new FileOutputStream(propertyFilePath);
            prop.setProperty("appActivity", "vodafone.vis.engezly.ui.screens.splash.SplashActivity");
            prop.store(output, null);

        } catch (Exception exp) {
            System.out.println(exp.getMessage());
            System.out.println(exp.getCause());

        }

    }

}
