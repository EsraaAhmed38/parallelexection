package Config;

public final class ExcelConfg {

    public final static String Path_ExcelSheets = ".//Inputs//";
    public final static String File_TestData_Input = "MSISDN.xlsx";
    public final static String SheetName_TestData = "Sheet1";
    public final static String SheetName_TestData1 = "Sheet2";
    public final static String SheetName_TestData2 = "Sheet3";
    public final static String File_TestData_Output = "TestResults.xlsx";
    public final static String SheetName_Output = "BalanceReport";


}
