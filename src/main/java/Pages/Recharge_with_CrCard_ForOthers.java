package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

public class Recharge_with_CrCard_ForOthers {
    public static WebElement getAmountField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/amountContainer");
    }

    public static WebElement getCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }

    public static WebElement getrechargeBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/done_btn");
    }


    public static WebElement getCrCardSaved(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnCreditCard");
    }

    public static WebElement getdesiredsavedCrCard(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/info");
    }

    public static WebElement getConfirmbyCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    public static WebElement getOKBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    //payment for others
    public static WebElement getRechargeforOthersBTN(final AndroidDriver driver) {
//	 return driver.findElementByAndroidUIAutomator(“new UiScrollable(new UiSelector()).scrollIntoView(text(“”))”);
        return driver.findElementById("com.emeint.android.myservices:id/serviceActionBtn");
    }

    public static WebElement getLandLine(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/mobileNumLayout");
    }

    public static WebElement getContactsBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/contacts_btn");
    }

    public static WebElement getAllowContactsBTN(final AndroidDriver driver) {
        return driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
    }

    public static WebElement getSearchField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/searchET");
    }

    public static WebElement getSelectedLandLinefield(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/selectedCB");
    }

    public static WebElement getSelectedLandLineBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/selectContact");
    }


}
