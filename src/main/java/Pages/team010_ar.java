package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class team010_ar {
    //open team010from home page
    public static WebElement openteam010_AR(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/cardLayoutBg");
    }

    public static WebElement AddMemberinFullMembersText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='فريقك مكتمل العدد']"));
    }

    public static WebElement AddMemberinFullMembersScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='انت وصلت للحد الأقصى لاضافة رقم جديد لفريقك ممكن تحذف حد من فريقك علشان تقدر تضيف الرقم ده']"));
    }

    //open team010 from side menu
    //open sidemenu
    public static WebElement openteam010sidemenu_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iwt_menu");

    }

    //open icon from side menu
    public static WebElement team010sidemenu_ar(final AndroidDriver driver) {
        return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.LinearLayout");
    }

    //
    public static WebElement startonboardingOffer_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvTopTitle");
    }

    public static WebElement startonboardingButton_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnAction");
    }

    public static WebElement allowaccessOffer_ar(final AndroidDriver driver) {
        return driver.findElementById("com.android.permissioncontroller:id/permission_message");

    }

    public static WebElement allowaccessButton_ar(final AndroidDriver driver) {
        return driver.findElementById("com.android.permissioncontroller:id/permission_allow_button");

    }

    public static WebElement denyaccess_ar(final AndroidDriver driver) {
        return driver.findElementById("com.android.permissioncontroller:id/permission_deny_button");

    }

    public static WebElement AddMembericon_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/itemCommunityMember");
    }

    public static WebElement AddMembermanual_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/etAddMemberManually");

    }

    public static WebElement AddMembercontact_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/ivAddFromContacts");
    }

    public static WebElement AddMembercontactSelection_ar(final AndroidDriver driver) {
        //return driver.findElementByXPath("com.emeint.android.myservices:id/selectedCB");
        return (WebElement) driver.findElements(By.className("android.widget.CheckBox")).get(0);
    }

    public static WebElement SelectRecieptContactbutton_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/selectContact");
    }

    public static WebElement Addbutton_ar(final AndroidDriver driver) {
//		return driver.findElementById("	com.emeint.android.myservices:id/btnAddMember");
//		return driver.findElementByXPath("com.emeint.android.myservices:id/selectedCB");
        return driver.findElement(By.xpath("//android.widget.Button[@text='اضافة للفريق']"));
    }

    public static WebElement Memberbutton1_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    //---------------------------Add Member Assertion ------------------------------------------------------------
    public static WebElement Team010PageTitleText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='فريق 010']"));
    }

    public static WebElement LeaveTeamTitleText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='تم بنجاح']"));
    }

    public static WebElement AddMemberText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='تم الاضافة بنجاح']"));
    }

    public static WebElement AddMemberTwiceText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='عفوا!']"));
    }

    public static WebElement AddMemberScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='تم اضافة حد من حبايبك في فريقك بنجاح']"));
    }

    public static WebElement AddMemberTwiceScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='الرقم ده موجود في فريقك حاليا']"));
    }

    public static WebElement AddNonEligibleMemberScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='رقم الموبايل ده غير متاح اضافته في فريقك']"));
    }

    public static WebElement AddNonVodafoneMemberScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='يمكنك اضافة أرقام فودافون فقط']"));
    }
    //----------------------------------------end--------------------------------------------------------------------

    //click on team member
    public static WebElement Memberbutton_ar(final AndroidDriver driver) {
        return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[2]/android.view.ViewGroup");
    }

    public static WebElement RemoveMemberbutton_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnRemove");
    }

    //------------------------Remove Member assertion-------------------------------------------------------------
    public static WebElement RemoveMemberText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='تم بنجاح']"));
    }

    public static WebElement RemoveMemberScript(final AndroidDriver driver, String num) {
//		return driver.findElementById("com.emeint.android.myservices:id/cardLayoutBg");
        return driver.findElement(By.xpath("//android.widget.TextView[@text='تم حذف " + num + " من فريقك بنجاح']"));

    }

    //-----------------------------------end------------------------------------------------------------
    public static WebElement OKRemoveMemberbutton_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    public static WebElement AddSameMemberTwice_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnVF");
    }

    public static WebElement AddNonVFMember_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnVF");
    }

    public static WebElement AddNotEligibleVFMember_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnVF");
    }

    public static WebElement TeamFul_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    public static WebElement TeamOfIcon_ar(final AndroidDriver driver) {
        return (WebElement) driver.findElements(By.className("android.widget.FrameLayout")).get(1);
        //return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/android.view.ViewGroup");
    }

    public static WebElement TeamOfLeave_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnRemove");
    }

    public static WebElement LeaveTeamSuccessOverlay_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    //------------------------Leave Team assertion-------------------------------------------------------------
    public static WebElement LeaveTeamText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='تم بنجاح']"));

    }

    public static WebElement LeaveTeamScript(final AndroidDriver driver, String num) {
//		return driver.findElementById("com.emeint.android.myservices:id/cardLayoutBg");
        return driver.findElement(By.xpath("//android.widget.TextView[@text='تم الخروج من فريق " + num + " بنجاح']"));
    }

    //-----------------------------------end------------------------------------------------------------
    public static WebElement sucessbutton_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");

    }

    public static WebElement NotEligibleRateplan_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnVF");
    }

    public static WebElement NotVF_ar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnVF");
    }

}

