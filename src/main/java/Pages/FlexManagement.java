package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class FlexManagement {

    public static WebElement getFlexManageTitlefinal(AndroidDriver driver) {
        return driver.findElement(By.xpath("//"));
    }

    public static WebElement getFlexManage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnAction");
    }

    public static WebElement getHomeRemainingFlex(final AndroidDriver driver) {
        return driver.findElementById("");
    }

    public static WebElement getManageRemainingFlex(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/remainingFlexesTextView");
    }

    public static WebElement getRepurchaseBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/flexRepurchaseBtn");
    }

    public static WebElement getTab(final AndroidDriver driver, String Tab) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='" + Tab + "']"));
    }

    public static WebElement getConsumptionTab(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tv_title"));
    }

    public static WebElement ARgetConsumptionTab(final AndroidDriver driver)//New
    {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='تفاصيل فليكساتك المستخدمة']"));

    }

    public static WebElement getBreakDownTab(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/remainingFlexesTextView"));
    }

    public static WebElement ARgetBreakDownTab(final AndroidDriver driver)// new
    {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='تفاصيل الفليكسات المتبقية من باقتك والعروض والخدمات الاخرى']"));
    }

    public static WebElement getCard(final AndroidDriver driver, String card) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='" + card + "']"));
    }

    public static WebElement getSetInternetLimit(final AndroidDriver driver) {
        //return driver.findElement(By.id("com.emeint.android.myservices:id/setLimitBtn[1]"));
        return driver.findElement(By.xpath("//android.widget.Button[@text='Set limit']"));
    }

    public static WebElement ARgetSetInternetLimit(final AndroidDriver driver)// new
    {
        return driver.findElement(By.xpath("//android.widget.Button[@text='اتحكم']"));
    }

    public static WebElement getSetXnetLimit(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.Button"));

    }

    public static WebElement getSetInternetLimitScript(final AndroidDriver driver)///////////////
    {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Set a limit of consuming flexes from your flex bundle on internet']"));
        //return driver.findElement(By.id("com.emeint.android.myservices:id/descTextView"));
    }

    public static WebElement ARgetSetInternetLimitScript(final AndroidDriver driver)//New
    {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='دلوقتي تقدر تتحكم في فليكساتك اللي بتستخدمها علي الانترنت من باقة فليكس']"));
        //return driver.findElement(By.id("com.emeint.android.myservices:id/descTextView"));
    }

    public static WebElement getSetXnetLimitScript(final AndroidDriver driver)////////////
    {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Set a limit of consuming flexes from your flex bundle to non-Vodafone numbers']"));
		/*return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
				+ "FrameLayout/android.widget.LinearLayout/android.widget."
				+ "FrameLayout/android.widget.RelativeLayout/android.widget."
				+ "LinearLayout/android.support.v4.widget.DrawerLayout/android.widget."
				+ "FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android."
				+ "widget.FrameLayout/android.view.View/android.support.v4.view.ViewPager/android."
				+ "widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
				+ "FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.view.View/android.widget."
				+ "FrameLayout/android.widget.LinearLayout/android.widget.TextView"));*/
    }

    public static WebElement ARgetSetXnetLimitScript(final AndroidDriver driver)//New
    {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='دلوقتي تقدر تتحكم في فليكساتك اللي بتستخدمها للشبكات الاخري من باقة فليكس']"));
    }

    public static WebElement getOverlayScript(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/flexLimitDesTextView"));
    }

    public static WebElement getOverlayRaminingFlex(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/remainingTextView"));
    }

    public static WebElement getOverlayTotalFlex(final AndroidDriver driver) {
        //return driver.findElement(By.id("com.emeint.android.myservices:id/totalTextView"));
        return driver.findElement(By.xpath("//android.widget.TextView[@text='0 Flexe']"));
    }

    public static WebElement ARgetOverlayTotalFlex(final AndroidDriver driver)// new
    {
        //return driver.findElement(By.id("com.emeint.android.myservices:id/totalTextView"));
        return driver.findElement(By.xpath("//android.widget.TextView[@text='0 فلكس']"));
    }

    public static WebElement getOverlaySetLimitText(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/setLimitEditText"));
    }

    public static WebElement getOverlaySetLimitBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/setLimitActionBtn"));
    }

    public static WebElement getOverlayInputErrorCode(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/inputTextError"));
    }

    public static WebElement getOverlaySuccessMessage(final AndroidDriver driver) {
//		return driver.findElement(By.id("com.emeint.android.myservices:id/titleTextView"));
        return driver.findElement(By.id("com.emeint.android.myservices:id/tvBorrowQuota"));
    }

    public static WebElement getOverlaySuccessScript(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/subtitleTextView"));
    }

    public static WebElement getOverlaySuccessDoneBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/actionButton"));
    }

    public static WebElement getSetInternetLimitProgressBar(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/usageProgressBar"));
    }

    public static WebElement getSetInternetLimitChangeLimitBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/optinButton"));
    }

    public static WebElement getSetInternetLimitOptoutBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/optoutButton"));
        //return driver.findElement(By.xpath("//android.widget.Button[@text='Opt out']"));
    }

    public static WebElement getSetInternetLimitRemaingFlex(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/cardRemainingAmountTextView"));
    }

    public static WebElement getSetInternetLimitTotalFlex(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/cardTotalAmountTextView"));
    }

    public static WebElement getSetXnetLimitProgressBar(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/usageProgressBar"));
    }

    public static WebElement getSetXnetLimitChangeLimitBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/optinButton"));
    }

    public static WebElement getSetXnetLimitOptoutBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/optoutButton"));
    }

    public static WebElement getSetXnetLimitRemaingFlex(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/cardRemainingAmountTextView"));
    }

    public static WebElement getSetXnetLimitTotalFlex(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/cardTotalAmountTextView"));
    }

    public static WebElement getWhatsappScript(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/cardTotalAmountTextView"));
    }

    public static WebElement getToggleBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/toggleButton"));
    }

    public static WebElement getSecondToggle(final AndroidDriver driver) {
		/*return driver.findElement(By.id("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
				+ "FrameLayout/android.widget.LinearLayout/android.widget."
				+ "FrameLayout/android.widget.RelativeLayout/android.widget."
				+ "LinearLayout/android.support.v4.widget.DrawerLayout/android.widget."
				+ "FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android."
				+ "widget.FrameLayout/android.view.View/android.support.v4.view.ViewPager/android."
				+ "widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android."
				+ "widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android."
				+ "view.View/android.widget.FrameLayout/android.widget.LinearLayout/android."
				+ "widget.RelativeLayout/android.widget.ToggleButton"));*/
//		return driver.findElement(By.xpath("//android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ToggleButton"));
        return (WebElement) driver.findElements(By.id("com.emeint.android.myservices:id/toggleButton")).get(0);

    }

    public static WebElement getWhatsappCardScript(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/descTextView"));
    }

    public static WebElement getOverlayConfirm(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/subtitleTextView"));
        //return driver.findElement(By.xpath("//android.widget.TextView[@text='Confirmation']"));
    }

    public static WebElement getOverlayConfirmBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/actionButton"));
    }

    public static WebElement getOnSuccessMessage(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/subtitleTextView"));
    }

    public static WebElement getOnSuccessDone(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/actionButton"));
    }

    public static WebElement getOverlayRecharge(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tv_text"));
    }


    public static WebElement getOverlayRechargeBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/button_positive"));
    }

    public static WebElement getOverlayRechargeCancelBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/button_negative"));
    }


    public static WebElement getWhatsappCardStatus(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/cardStatusTextView"));
    }

    public static WebElement getWhatsappProgressBar(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/usageProgressBar"));
    }

    public static WebElement whatsacript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[1][@text='واتساب بلا حدود :']"));
    }


}
