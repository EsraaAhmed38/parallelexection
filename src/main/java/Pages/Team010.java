package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Team010 {
    //open team010from home page
    public static WebElement openteam010(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/cardMainContainer");
    }

    public static WebElement Team010PageTitleText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Team 010']"));
    }

    public static WebElement LeaveTeamTitleText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Success']"));
    }

    public static WebElement AddMemberTwiceText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Sorry!']"));
    }

    public static WebElement AddMemberinFullMembersText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Your team is full!']"));
    }

    public static WebElement AddMemberinFullMembersScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='You have reached the maximum number of members in your team. Remove one of your existing members to be able to add this member.']"));
    }

    public static WebElement AddNonEligibleMemberScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='This mobile number is not eligible to be added to your team']"));
    }

    public static WebElement AddMemberTwiceScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='This number is already member in your team']"));
    }

    public static WebElement AddNonVodafoneMemberScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='You can only add Vodafone numbers']"));
    }

    //open team010 from side menu
    public static WebElement openteam010sidemenu(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iwt_menu");

    }

    public static WebElement team010sidemenu(final AndroidDriver driver) {
        return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout");
    }


    public static WebElement startonboarding(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnAction");
    }

    public static WebElement allowaccess(final AndroidDriver driver) {
        return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]");

    }

    public static WebElement denyaccess(final AndroidDriver driver) {
        return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[2]");
    }

    public static WebElement AddMemberText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Member added!']"));
    }

    public static WebElement RemoveMemberText(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Removed!']"));
    }

    public static WebElement AddMemberScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='You have successfully added a new member to your team']"));
    }

    public static WebElement AddMembericon(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/itemCommunityMember");
    }

    public static WebElement AddMembermanual(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/etAddMemberManually");
    }

    public static WebElement AddMembercontact(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/ivAddFromContacts");
    }

    public static WebElement AddMembercontactSelection(final AndroidDriver driver) {
        return (WebElement) driver.findElements(By.className("android.widget.CheckBox")).get(0);

    }

    public static WebElement SelectRecieptContactbutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/selectContact");
    }

    public static WebElement Addbutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnAddMember");
    }

    public static WebElement Memberbutton1(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/itemCommunityMember");
    }

    public static WebElement Memberbutton(final AndroidDriver driver) {
        return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[2]/android.view.ViewGroup");
    }

    public static WebElement RemoveMemberbutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnRemove");
    }

    public static WebElement OKRemoveMemberbutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    public static WebElement AddSameMemberTwice(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnVF");
    }

    public static WebElement AddNonVFMember(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnVF");
    }

    public static WebElement AddNotEligibleVFMember(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnVF");
    }

    public static WebElement TeamFul(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    public static WebElement TeamOfIcon(final AndroidDriver driver) {
        return (WebElement) driver.findElements(By.className("android.widget.FrameLayout")).get(1);

//		return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]");
    }

    public static WebElement TeamOfLeave(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnRemove");
    }

    public static WebElement LeaveTeamSuccessOverlay(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    public static WebElement sucessbutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    public static WebElement NotEligibleRateplan(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnVF");
    }

    public static WebElement NotVF(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnVF");
    }
//	public static WebElement PowerLow(final AndroidDriver driver) {
//		return driver.findElementByLinkText("Low");
//	}
//	public static WebElement ZeroinTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (0/10)");
//	}
//	public static WebElement OneinTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (1/10)");
//	}
//	public static WebElement TwoinTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (2/10)");
//	}
//	public static WebElement PowerMedium(final AndroidDriver driver) {
//		return driver.findElementByLinkText("Medium");
//	}
//	public static WebElement ThreeinTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (3/10)");
//	}
//	public static WebElement FourinTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (4/10)");
//	}
//	public static WebElement FiveinTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (5/10)");
//	}
//	public static WebElement PowerHigh(final AndroidDriver driver) {
//		return driver.findElementByLinkText("High");
//	}
//	public static WebElement SixinTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (6/10)");
//	}
//	public static WebElement SeveninTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (7/10)");
//	}
//	public static WebElement EightinTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (8/10)");
//	}
//	public static WebElement NineinTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (9/10)");
//	}
//	public static WebElement TeninTeam(final AndroidDriver driver) {
//		return driver.findElementByLinkText("My Team (10/10)");
//	}
}

