package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

public class AddSpacesAR {


    public static WebElement getflexpage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/renewalLayout");
    }

    public static WebElement getADDSpacesCard(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/root");
    }

    public static WebElement getrecharge(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvNumber");
    }

    public static WebElement getrechargebalance(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/title");
    }

    public static WebElement getbalancedetails(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='تفاصيل الرصيد']");

    }

    public static WebElement getrechargebalancebytext(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='شحن الرصيد']");
    }

    public static WebElement getdonebutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnYes");
    }

    public static WebElement getNObutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnNo");
    }

    public static WebElement getaddSpacesCard(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/root");
    }


    public static WebElement getCARDOFFER(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvSubTitle");
    }
	/*public static WebElement getRechargeAndBalance(final AndroidDriver driver) {

		return driver.findElement(By.xpath("//android.widget.TextView[@text='إشحن']"));
		
	}*/

    public static WebElement getRechargeAndBalanc(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnRight");

    }

    public static WebElement getofferdetails(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvSubTitle");

    }

    public static WebElement Redeemoffer(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnYesContainer");

    }

    public static WebElement Rejectoffer(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnNo");
    }

    public static WebElement DenyContact(final AndroidDriver driver) {
        return driver.findElementById("com.android.packageinstaller:id/permission_deny_button");
    }

    public static WebElement commitbtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnYesContainer");
    }

    public static WebElement Rechargebutton2(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/icThumbnail");
    }


}
