package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;


public class DonationsPage {
    public static WebElement getLanguagePage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvOnboardingHeader");
    }


    public static WebElement getLanguage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/imgLanguage");
    }

    public static WebElement getSeamless(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnSeamless");
    }


    public static WebElement getSideMenu(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_side_menu");
    }

    public static WebElement getVodafoneCashEn(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Vodafone Cash']");
    }

    public static WebElement getVodafoneCashAr(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='فودافون كاش']");
    }

    public static WebElement getDonations(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/cash_donation_service");
    }

    public static WebElement getResalah(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='مؤسسة رسالة']");
    }

    public static WebElement getMagdy(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='مؤسسة مجدي يعقوب']");
    }

    public static WebElement getBankElt3am(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='مؤسسة بنك الطعام']");
    }

    public static WebElement getBaheya(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='مستشفى بهية']");
    }

    public static WebElement getJan25(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='مستشفى 25يناير']");
    }

    public static WebElement get57357(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='مستشفى 57357']");
    }

    public static WebElement getMasrElkher(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='مؤسسة مصر الخير']");
    }

    public static WebElement getPassword(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/passwordEditTxt");
    }

    public static WebElement getAmount(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/amountEditTxt");
    }

    public static WebElement getDonateBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/donateBtn");
    }

    public static WebElement getExitBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/closeSuccessLayoutBtn");
    }

    public static WebElement getSideMenuinside(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/toolbar_menu_icon");
    }


    public static WebElement getThankYouMSG(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/textView10");
    }
}
