package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PaymentDslPage {

    public static WebElement getBillAndPayment(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/mainParentView"));
    }

    public static WebElement getpay_Mybill(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My Bill ']"));
    }

    public static WebElement getPayNow(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnPayNow");
    }

    public static WebElement getaddNewCard(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/addCreditCardBtn");
    }

    public static WebElement getaddCardHolder(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_card_name");
    }

    public static WebElement getaddCardNumber(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_card_number");
    }

    public static WebElement getaddCardYear(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='YY']"));
    }

    public static WebElement getaddCardYearInput(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='2022']"));
    }

    public static WebElement getaddCardMonth(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='MM']"));
    }

    public static WebElement getaddCardMonthInput(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='07']"));
    }

    public static WebElement getMybill_Home(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My Bill']"));
    }

    public static WebElement getaddCardCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }

    public static WebElement getaddCardConfirmation(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_add");
    }

    public static WebElement getConfirmAddedCard(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='OK']"));
    }

    public static WebElement getaddCardScript(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Card registered successfully']"));
    }

    public static WebElement getConfirmFailedCard(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/vb_operation");
    }

    public static WebElement getCardsListDropdown(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnCreditCard");
    }

    public static WebElement getAddCardDropdown(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_addCreditCard");
    }

    public static WebElement getpay_bill_for_others(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Pay bill for others']"));
    }

    public static WebElement getpayRechargeForOthers(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge for others']"));
    }

    public static WebElement getPaymentHistory(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Payment History']"));
    }

    public static WebElement getTopReports(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Top reports']"));
    }

    public static WebElement getUnbilledAmount(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Unbilled amount']"));
    }

    public static WebElement getDetailedConsumption(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Detailed consumption']"));
    }

    public static WebElement getManageMyCards(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tv_manageCreditCard"));
    }

    public static WebElement getdeleteCards(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/iv_deleteCard"));
    }

    public static WebElement getPaymentNow(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/doneBtn"));
    }

    //////////////////////// prepaid/////////////////////////////////////////////////////

    public static WebElement getRecharge_Home(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge']"));

    }

    public static WebElement getRechargeAndBalance(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge and Balance']"));

    }

    public static WebElement getAmountToPay(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/leftSideInput");
    }

    public static WebElement getBalanceToPay(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/rightSideInput");
    }

//	public static WebElement getBalanceincard(final AndroidDriver driver) {
//		return driver.findElementById(" com.emeint.android.myservices:id/rightSideInput");
//	}


    public static WebElement getRechargeConfrmation(final AndroidDriver driver) {

        //return driver.findElement(By.id("com.emeint.android.myservices:id/btn_confirm']"));
        return driver.findElement(By.xpath("//android.widget.Button[@text='Confirm']"));
    }


    public static WebElement getRecharge(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge']"));
    }

    public static WebElement getRechargeForOthers(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge for others']"));
    }

    public static WebElement getBalanceDetails(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Balance details']"));
    }

    public static WebElement getADDNEWCARD(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='ALI MOHAMED GAMEL HASSAN']"));
    }

    public static WebElement getmorethanCrCardsection(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Credit Card']");
    }

    public static WebElement getselectcard(final AndroidDriver driver) {

        return driver.findElementById("com.emeint.android.myservices:id/creditCardName");
    }

    public static WebElement AddcardFromDropMenu(final AndroidDriver driver) {

        // return
        // driver.findElementById("com.emeint.android.myservices:id/header");
        return driver.findElementById("com.emeint.android.myservices:id/btnCreditCard");
    }

    public static WebElement getscratchCard(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Scratch Card']");
    }

    public static WebElement getvodafoneCash(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Vodafone Cash']");
    }

    public static WebElement getconfirmBTN(final AndroidDriver driver) {
        // return
        // driver.findElementByXPath("//android.widget.TextView[@text='Confirm']");
        return driver.findElementById("com.emeint.android.myservices:id/alertButtonsLayout");
    }

    public static WebElement getCardsList(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/rv_CreditCard");
    }
    //////////////////////////////////////////////////////// try

    public static WebElement getCrCardspecificExpMonthField(final AndroidDriver driver, String month) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='" + month + "']"));
    }

    public static WebElement getCrCardspecificExpYearField(final AndroidDriver driver, String yaer) {
        return driver.findElementByXPath("//android.widget.CheckedTextView[@text='" + yaer + "']");
    }

////////////////////////////////////////////////DSL For Self 


    public static WebElement getscratchcard(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Scratch Card']");
    }

    public static WebElement getScratchCardNumber(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/etScratchCardNumber");
        //return driver.findElementById("com.emeint.android.myservices:id/mobileNumLayout");
    }

    public static WebElement getScratchCardOthersNumber(final AndroidDriver driver) {
        //return driver.findElementById("com.emeint.android.myservices:id/et_scratch_card");
        return driver.findElementById("com.emeint.android.myservices:id/etScratchCardNumber");
    }

    public static WebElement getrechargeBTN(final AndroidDriver driver) {
//return driver.findElementById("com.emeint.android.myservices:id/done_btn");
        return driver.findElementById("com.emeint.android.myservices:id/btnRechargeScratchCard");
    }

    public static WebElement getSnackBar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/snackbar_text");

    }

    /////////////////////////DSL For Cc
    public static WebElement getAmountField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/amountContainer");
    }

    public static WebElement getCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }


    public static WebElement getCrCardSaved(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnCreditCard");
    }

    public static WebElement getdesiredsavedCrCard(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/info");
    }

    public static WebElement getConfirmbyCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    public static WebElement getOKBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    //payment for others
    public static WebElement getRechargeforOthersBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/serviceActionBtn");
    }

    public static WebElement getLandLine(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_mobile_number");
    }

    public static WebElement getContactsBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/contacts_btn");
    }

    public static WebElement getAllowContactsBTN(final AndroidDriver driver) {
        return driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
    }

    public static WebElement getSearchField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/searchET");
    }

    public static WebElement getSelectedLandLinefield(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/selectedCB");
    }

    public static WebElement getSelectedLandLineBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/selectContact");
    }

////////CC for self


    public static WebElement getfailOKBTN(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.Button[@text='OK']");
    }

    public static WebElement getAddCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }


    public static WebElement getDeleteCardicon(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_deleteCard");
    }

    public static WebElement getDeleteconfirmationoption(final AndroidDriver driver) {
        return driver.findElementById("android:id/button1");
    }

    //find "no" option from confirmation delete
    public static WebElement getNotDeleteconfirmationoption(final AndroidDriver driver) {
        return driver.findElementById("android:id/button2");
    }

    //Get Recharge BTN once click on ADSL from side menu
    public static WebElement getADSLRechageBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/walletRechargeBtn");
    }

    public static WebElement getErrorMsgforwrongCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/alertSecondaryText");
    }

    public static WebElement getAddNewCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/addCreditCardBtn");
    }

    public static WebElement getCrCardnameField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_card_name");
    }

    public static WebElement getCrCardNumberField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_card_number");
    }

    public static WebElement getCrCardExpYearField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='YY']");
    }

    public static WebElement getCrCardspecificExpYearField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.CheckedTextView[@text='2021']");
    }

    public static WebElement getCrCardspecificExpMonthField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.CheckedTextView[@text='09']");
    }

    public static WebElement getCrCardExpMonthField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='MM']");
    }

    public static WebElement getCrCardCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }

    public static WebElement getAddThisCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_add");
    }

    public static WebElement getErrorMsgfornotaddedCrCard(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Sorry! Your transaction failed, please try again or contact your bank for more assistance']");
    }

    public static WebElement getCongratulationstext(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Congratulations']");
    }

    public static WebElement getCardregisteredsuccessfullytext(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Card registered successfully']");
    }

    //Receipt info
    public static WebElement getCardReceiptdoneBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_print");
    }

    public static WebElement getCardReceiptlandline(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_mobile_number");
    }

    public static WebElement getCardReceiptdate(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_date");
    }

    public static WebElement getCardReceiptsuccessmsg(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/titleDesc");
    }

    public static WebElement getCardReceiptamount(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_bill_amount");
    }


}


