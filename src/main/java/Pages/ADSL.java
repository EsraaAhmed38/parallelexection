package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;


public class ADSL {

    public static WebElement getSideMenu(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_side_menu");
    }


    public static WebElement getADSL(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='DSL']");
    }

    public static WebElement getSubscribeToAdsl(final AndroidDriver driver) {
        return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.FrameLayout/android.widget.RelativeLayout");
    }

    public static WebElement getSubscribeToSpeed(final AndroidDriver driver) {
        return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.view.View/android.widget.Button");
    }

    public static WebElement getFirstname(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/eTxtFirstName");
    }

    public static WebElement getFirstnameError(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/firstNameErrorMsg");

    }

    public static WebElement getLastName(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/eTxtLastName");
    }

    public static WebElement getLastNameError(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/lastNameErrorMsg");

    }

    public static WebElement getCityCode(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/eTxtCityCode");
    }

    public static WebElement getCityCodeError(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/cityCodeErrorMsg");

    }

    public static WebElement getLandline(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/eTxtLandLineNo");
    }

    public static WebElement getlandlineError(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/landLineNoErrorMsg");

    }

    public static WebElement getVodafoneNumber(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/eTxtMsisdn");
    }

    public static WebElement getVodafoneNumberError(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/msisdnErrorMsg");

    }

    public static WebElement getPrefferedCheck(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/preferedCheckMark");
    }

    public static WebElement getPrefferedNumber(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/eTxtPreferedNo");
    }

    public static WebElement getPrefferedNumberError(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/preferedNoErrorMsg");

    }


    public static WebElement getRegisterBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/adslRegisterBtn");

    }

    public static WebElement getOverFullName(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvFullNameValue");

    }

    public static WebElement getOverVodafoneNo(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvVfNoValue");

    }

    public static WebElement getOverPrefferdNo(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvPreferredNoValue");

    }

    public static WebElement getLandLineNo(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvLandlineNoValue");

    }

    public static WebElement getExitOverlay(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_exit");
    }

    public static WebElement getConfirmOverlay(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/almostDoneConfirmBtn");
    }


    public static WebElement getSrExistOverlay(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/alertSecondaryText");
    }

}
