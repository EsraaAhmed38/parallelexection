package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DeepLinks {
    public static WebElement getCard(final AndroidDriver driver, String card) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='" + card + "']"));
    }

    public static WebElement getId(final AndroidDriver driver, String card) {
        return driver.findElement(By.id(card));
    }
}
