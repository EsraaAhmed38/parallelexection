package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PopUpsPage {

    //// Rate App On google play
    public static WebElement getYesButton_rate(final AndroidDriver driver) {

        return driver.findElement(By.id("android:id/button1"));

    }

    public static WebElement checkExistenceOfLogo(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/iv_side_logo"));

    }


    public static WebElement getNoButton_Rate(final AndroidDriver driver) {

        return driver.findElement(By.id("android:id/button1"));

    }


    public static WebElement getAlertRateMessage(final AndroidDriver driver) {

        return driver.findElement(By.id("android:id/alertTitle"));

    }

    public static WebElement cancelSurvey(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/survey_cancel_btn"));

    }

    public static WebElement getRemindmelaterButton_Rate(final AndroidDriver driver) {

        return driver.findElement(By.id("android:id/button3"));

    }

    //// Redeem 400 MB from Home page

    public static WebElement getRedeemButton_Offer(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.Button[@text='Okay']"));
    }

	/*public static WebElement getSkipButton_offer(final AndroidDriver driver) {

		return driver.findElement(By.xpath("//android.widget.TextView[@text='Skip']"));
	}*/

    public static WebElement getSkipButton_offerAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='تخطي']"));
    }

    public static WebElement getSkipButtonOffer(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/laterTv"));
    }

}