package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Sidemenu {

////get side_menu_elements

    public static WebElement getClose(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/close_btn");
    }

    public static WebElement getBack(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/toolbar_backIcon");
    }

    public static WebElement getHome(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Home']");
    }

    public static WebElement getHomePage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/fab");
    }

    public static WebElement getMyPlan(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='My plan']");
    }

    public static WebElement getHomeSidemenuId(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iwt_menu");
		
		/* List<WebElement> myList=driver.findElements(By.id("com.emeint.android.myservices:id/ivIcon"));
		 WebElement sideMenu=myList.get(4);
		 System.out.println(sideMenu);
		 return sideMenu;*/

    }

    public static WebElement getSidemenu(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iwt_menu");
    }

    public static WebElement getElementSideMenu(final AndroidDriver driver, String sideMenuName) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='" + sideMenuName + "']"));
    }


    public static WebElement getMobileInternet(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Mobile internet']");
    }

    public static WebElement getMobileInternetPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Mobile Internet Management']");
    }

    public static WebElement getFlex365(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Flex 365']");
    }

    public static WebElement get365offersPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='365 Offers']");
    }

    public static WebElement getSallefnyShokran(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Sallefny shokran']");

    }

    public static WebElement getSallefnyShokranPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Sallefny Shokran']");

    }

    public static WebElement getBalanceTransfer(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Balance transfer']");
    }

    public static WebElement getBalanceTransferPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Balance Transfer']");
    }

    public static WebElement getRechargeandBalace(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Recharge and Balance']");
    }

    public static WebElement getManageFlex(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Manage Flex']");
    }

    public static WebElement getFlexManagementPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Flex Management']");
    }

    // not exist in flex
    public static WebElement get365Offers(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='365 Offers']");
    }

    public static WebElement getTeam010(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Team010']");
    }

    public static WebElement getTeam010Page(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvTitle");
    }

    public static WebElement getVodafonecash(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Vodafone Cash']");
    }

    public static WebElement getDSL(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='DSL']");
    }

    public static WebElement getServices(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Services']");
    }

    public static WebElement getUSB(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='USB']");
    }

    public static WebElement getUSBPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='My USB']");
    }

    public static WebElement getWhitelist(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Whitelist']");
    }

    public static WebElement getWhitelistPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Whitelist Service']");
    }

    public static WebElement getBlacklist(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Blacklist']");
    }

    public static WebElement getBlacklistPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Blacklist Service']");
    }

    public static WebElement getMissedCallKeeper(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Missed Call Keeper']");
    }

    public static WebElement getMyRedFamily(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='My Red Family']");
    }

    public static WebElement getMyRedFamilyPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='FAMILY']");
    }

    public static WebElement get3ala7esaby(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='3ala 7esaby']");
    }

    public static WebElement getRoaming(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Roaming']");
    }

    public static WebElement getEdfa3ly(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Edfa3ly shokran']");
    }

    public static WebElement getEdfa3lyPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Edfa3ly Shokran']");
    }

    public static WebElement getKallemny(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Kallemny Shokran']");
    }

    public static WebElement getShop(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Shop']");
    }

    public static WebElement getStore(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Store Locator']");
    }

    public static WebElement getSIM(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='SIM Swap']");
    }

    public static WebElement getSIMPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='4G SIM swap']");
    }

    public static WebElement getEntertainment(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Entertainment']");
    }

    public static WebElement getVodafone_Fantasy(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Vodafone Fantasy']");
    }

    public static WebElement getAlerting_services(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Alerting services']");
    }

    public static WebElement getPlayWin(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Play & win']");
    }

    public static WebElement getMohamedSalah(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Mohamed Salah World']");
    }

    public static WebElement getMohamedSalahPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.view.View[@content-desc='عالم محمد  صلاح']");
    }

    public static WebElement getModarag_Vodafone(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Modarag Vodafone']");
    }

    public static WebElement getVodafoneTv(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Vodafone TV']");

    }

    public static WebElement getVodafoneTvPage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/webView");

    }

    public static WebElement getSettings_Support(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Settings & Support']");

    }

    public static WebElement getSettings(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Settings']");

    }

    public static WebElement getSettingsPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Language']");

    }

    public static WebElement getMyProfile(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='My profile']");

    }

    public static WebElement getMyProfilePage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Gender']");

    }

    //////////////////////////////// RED////////////////////////////////////////////////////
    public static WebElement getMyBill(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='My Bill ']");
    }

    public static WebElement getMyBillPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Bill Management']");
    }

    public static WebElement getBill_Payments(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Bills and Payments']");
    }

    public static WebElement getRedServices(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Red services']");
    }

}


