package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RedMiManagement {

    public static WebElement getRefreshBtn(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/btnRefresh"));
    }

    public static WebElement getExtremeBundles(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Extreme bundles']"));
    }

    public static WebElement getSuperPassBundles(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Super pass']"));
    }

    public static WebElement getTimeBasedBundles(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Time Based']"));
    }

    public static WebElement getContentBundles(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Content']"));
    }

    public static WebElement getAddOnsBundles(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Add-Ons']"));
    }

    public static WebElement get1800MB(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='1800 MB On demand']"));
    }

    public static WebElement get2500MB(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='2500 MB On demand']"));
    }

    public static WebElement get4000MB(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='4000 MB On demand']"));
    }

    public static WebElement get5000MB(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='5000 MB On demand']"));
    }

    public static WebElement get8000MB(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='8000 MB On demand']"));
    }

    public static WebElement get12000MB(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='12000 MB On demand']"));
    }

    public static WebElement get20000MB(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='20000 MB On demand']"));
    }

    public static WebElement get40000MB(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='40000 MB On demand']"));
    }

    public static WebElement getsubscribeBtn(final AndroidDriver driver) {

        return driver.findElement(By.xpath(
                "//android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.Button"));
    }

    public static WebElement getSubscribeScript(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/alertPrimaryText"));
    }

    public static WebElement getCancelBtn(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.Button[@text='Cancel']"));
    }

    public static WebElement getConfirmBtn(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.Button[@text='Confirm']"));
    }

    public static WebElement getRenwalBundle(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/btnCardAction"));
    }
}
