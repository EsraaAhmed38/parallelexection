package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;


public class Migration_Platform_postpaid_AR {
    //side menu
    public static WebElement getMyPlanTap(final AndroidDriver driver) {
        return driver.findElementByXPath("//androidx.appcompat.app.ActionBar.Tab[@content-desc='دقائق']");
    }

    public static WebElement getOpsButton(final AndroidDriver driver) {
        return driver.findElementById("om.emeint.android.myservices:id/doneBtn");
    }

    public static WebElement getExplorebutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnExploreMorePlans");
    }

    public static WebElement getUPgrade(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/vfbtnUpgrade");
    }

    public static WebElement getUPgradeconfirm(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    public static WebElement getUPgradelater(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnSecondary");
    }

    public static WebElement sucesstxt(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/titleTv");
    }

    public static WebElement donebutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    public static WebElement sucessscriptdetails(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/messageTv");
    }
}