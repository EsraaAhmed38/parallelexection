package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class Switch_Account_Page {


    public static WebElement getProfile(final AndroidDriver driver) {
        //return driver.findElementById("com.emeint.android.myservices:id/iv_switch_account") ;
        return driver.findElementById("com.emeint.android.myservices:id/iwt_profile");
    }

    public static WebElement getSwitchBtn(final AndroidDriver driver) {
        //return driver.findElementById("com.emeint.android.myservices:id/iv_switch_account") ;
        return driver.findElementById("com.emeint.android.myservices:id/btnSwitch");
    }

    public static WebElement getAddNewAccBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/addNewAccountBtn"));
    }

    public static WebElement getMobileNumField(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='Mobile Number']"));
    }

    public static WebElement getMobileNumFieldAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='رقم الموبايل']"));
    }

    public static WebElement getPwField(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='Password']"));
    }

    public static WebElement getPwFieldAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='كلمه المرور']"));
    }

    public static WebElement getAddAccountBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/login_btn"));
    }

    public static WebElement getSideMenu(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/iv_side_menu"));
    }

    public static WebElement getAccTobeSwitchTo(final AndroidDriver driver, String Number) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='" + Number + "']"));
//		return (WebElement) driver.findElements(By.className("android.widget.LinearLayout")).get(0);
    }

    public static WebElement getAnotherAccToSwitchTo(final AndroidDriver driver) {
        return (WebElement) driver.findElements(By.className("android.widget.LinearLayout")).get(1);
    }

    public static WebElement getThirdAccToSwitchTo(final AndroidDriver driver) {
        return (WebElement) driver.findElements(By.className("android.widget.LinearLayout")).get(2);
    }

    public static WebElement getErrorMsg(final AndroidDriver driver) {
        return driver.findElement(By.id("android:id/message"));
    }

    public static WebElement getOkButton(final AndroidDriver driver) {
        return driver.findElement(By.id("android:id/button1"));
        //return driver.findElement(By.xpath("//android.widget.Button[@text='OK']")) ;
    }

    public static WebElement getVfAccounts(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My Vodafone accounts'"));
    }

    public static WebElement getDeleteBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnDeleteAccount"));
    }

    public static WebElement getYesBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("android:id/button1"));
    }

    public static WebElement getMyProfile(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My profile']"));
    }

    public static WebElement getLogoutBtn(final AndroidDriver driver) {
        //return driver.findElement(By.id("com.emeint.android.myservices:id/btnLogOut")) ; //changed in the new build
        return driver.findElementByXPath("//android.widget.TextView[@text='تسجيل الخروج']");
    }

    public static WebElement getLogoutBtnEn(final AndroidDriver driver) {
        //return driver.findElement(By.id("com.emeint.android.myservices:id/btnLogOut")) ; //changed in the new build
        return driver.findElementByXPath("//android.widget.TextView[@text='Logout']");
    }

    public static WebElement getEditProfileBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnEdit"));
    }

    public static WebElement getOKBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("android:id/button1"));
    }

    public static WebElement getcloseBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/ivClose"));
    }
}