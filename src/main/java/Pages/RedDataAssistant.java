package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RedDataAssistant {


    public static WebElement getMIManagementPageFromHome(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/seekArc"));
    }

    public static WebElement getMIManagementPage(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Mobile internet']"));
    }

    public static WebElement getAfterBuundleOption(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvManageAfterBundleOption"));


    }

    public static WebElement getEnableTheToggle(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/switchServiceAddon"));
    }

    public static WebElement getKnowmoreBtn(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.Button[@text='Know more']"));
    }

    public static WebElement getKnowmoreBtnAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.Button[@text='اعرف اكتر']"));
    }

    public static WebElement getConfirmBtn(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.Button[@text='Confirm']"));
    }

    public static WebElement getConfirmBtnAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.Button[@text='تأكيد']"));
    }

    public static WebElement getDoneBtn(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/btnDone"));
    }

    public static WebElement getDisableToggle(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.Switch[@text='ON']"));
    }

    public static WebElement getDisableToggleAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.Switch[@text='تشغيل']"));
    }

    public static WebElement getPreviousBtn(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/btnPrevious"));

    }

    public static WebElement getNextBtn(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/btnNext"));
    }

    public static WebElement seeDtailedUsage(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/redirectionBtn"));
    }

    public static WebElement needHelpBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnNeedHelp"));
    }

}
