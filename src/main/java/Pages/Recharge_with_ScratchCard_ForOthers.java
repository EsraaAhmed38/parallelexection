package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

public class Recharge_with_ScratchCard_ForOthers {
    public static WebElement getscratchcard(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Scratch Card']");
    }

    public static WebElement getScratchCardNumber(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_scratch_card");
    }

    public static WebElement getrechargeBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/done_btn");
    }


}
