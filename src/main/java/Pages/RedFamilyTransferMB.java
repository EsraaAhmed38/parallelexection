package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RedFamilyTransferMB {

    public static WebElement getSwitchToTransfer(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='TRANSFER']"));
    }

    public static WebElement getEnterMB(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/megabytesEt"));
    }

    public static WebElement getClickOnFamilyMembers(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tv_main_spinner_text"));
    }

    public static WebElement getChooseFamilyMember(final AndroidDriver driver) {


        //return driver.findElement(By.xpath("//android.widget.TextView[@text='01066725177']"));

        return driver.findElement(By.xpath("//android.widget.TextView[@text='01010467050']"));

    }

    public static WebElement getSendMBAction(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/transferBtn"));
        //return driver.findElement(By.xpath("//android.widget.Button[@text='Send Megabytes']"));
        //return driver.findElement(By.xpath("//android.widget.Button[@text='ابعت الميجابايتس']"));
    }

    public static WebElement getSuccessBtn(final AndroidDriver driver) {

        return driver.findElement(By.id("android:id/button1"));
    }

    public static WebElement getBackBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/toolbar_backIcon"));
    }

    public static WebElement getremainMB(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/remainingTv"));
    }

    public static WebElement gettransferlMB(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/megabytesEt"));
    }

    public static WebElement getCreateFamilyBtn(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/addMemberFab"));
    }

    public static WebElement getEnterFamilyName(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/eet_family_name"));
    }

    public static WebElement getChooseNumber(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/eet_member_name"));
    }

    public static WebElement getAddMemberBtn(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/b_add_member_family"));
    }

    public static WebElement getConfirmAddingMember(final AndroidDriver driver) {

        return driver.findElement(By.id("android:id/button1"));
    }

    public static WebElement getSwitchToTransferAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='تحويل']"));
    }

}
