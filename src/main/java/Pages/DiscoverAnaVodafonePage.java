package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DiscoverAnaVodafonePage {
    public static WebElement Discoveranavodafonebuutton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tvDiscoverAnaVodafone"));

    }

    public static WebElement Shopbuutton(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Shop']"));

    }

    public static WebElement Entertainmentbuutton(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Entertainment']"));

    }

    public static WebElement StoreLocatorbuutton(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Store Locator']"));

    }

    public static WebElement FollowUsbuutton(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Follow Us']"));

    }
}
