package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Profile_Page {


    public static WebElement getProfileicon(final AndroidDriver driver) {
        //return driver.findElement(By.id("com.emeint.android.myservices:id/iv_switch_account"));
        return driver.findElementById("com.emeint.android.myservices:id/iwt_profile");
    }

    public static WebElement getSwitchButton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnSwitch"));
    }

    public static WebElement getEditButton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnEdit"));
    }

    public static WebElement getLanguageArrow(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/spinner_item_name"));
    }

    public static WebElement getLogout(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnLogOut"));
    }

    public static WebElement getCustomerName(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tvUserName"));
    }

    //MGM Part
    public static WebElement getEnterPromoCodeButton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnEnterPromoCode"));
    }

    public static WebElement getPromoCodeBox(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/promoCodeEdt");

    }

    public static WebElement getGitYourGiftButton(final AndroidDriver driver) {
        //return driver.findElementByXPath("//android.widget.TextView[@text='	Get your gift']");
        return driver.findElement(By.id("com.emeint.android.myservices:id/btn_getGift_primary"));
        //return driver.findElement(By.id("com.emeint.android.myservices:id/mgmWinView"));

    }

    public static WebElement getGiftTitle(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_title");
    }

    public static WebElement getGiftMsg(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_second_desc");
    }

    public static WebElement getGoConsumptionButton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_getGift_primary");
    }

    public static WebElement get_consumption_screen_gift_Title(final AndroidDriver driver) {
        //return driver.findElementById("com.emeint.android.myservices:id/titleTv");
        return driver.findElementByXPath("//android.widget.TextView[@text='Megabytes from promo code offer']");
    }

    public static WebElement getErrorCode(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/errorTextView");
    }

    public static WebElement getGitYourAGiftButton(final AndroidDriver driver) {
        //return driver.findElementByXPath("//android.widget.TextView[@text='	Get your gift']");
        return driver.findElement(By.id("com.emeint.android.myservices:id/mgmWinView"));

    }

    public static WebElement get_consumption_A_DoneBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }

    public static WebElement get_consumption_A_Title(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/titleTv");
    }

    public static WebElement get_consumption_A_Desc(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/messageTv");
    }


}




