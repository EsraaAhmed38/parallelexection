package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ADSLStatusPage {
    public static WebElement getaddmorequota(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/getMoreAddonsBtn"));

    }

    public static WebElement getbuyaddons(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnCardAction"));

    }

    public static WebElement getsuccefulmessageofbuyaddons(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/doneBtn"));
    }

    public static WebElement getsuspendedtatus(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Suspended']"));
    }

    public static WebElement getrechargebutton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/walletRechargeBtn"));
    }

    public static WebElement getpaymenthistorybutton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/walletPaymentHistoryBtn"));
    }

    public static WebElement getchangeratepanbutton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/itemBtn"));
    }

    public static WebElement getrechargeforothersbutton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/serviceActionBtn"));
    }

    public static WebElement getverfiyrechargeforotherspage(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Choose a convenient payment method']"));
    }

    public static WebElement getverfiyrechargewalletpage(final AndroidDriver driver) {
        return driver.findElement(By.xpath("com.emeint.android.myservices:id/tv_balance_amount"));
    }

    public static WebElement getbackbutton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/toolbar_backIcon"));
    }

    public static WebElement getretactivebutton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/getMoreAddonsBtn"));
    }

    public static By getActivestatus(final AndroidDriver driver) {
        return (By.xpath("//android.widget.TextView[@text='Active']"));
    }

    public static By getSuspendedstatus(final AndroidDriver driver) {
        return (By.xpath("//android.widget.TextView[@text='Suspended']"));
    }

    public static By getMoreaddons(final AndroidDriver driver) {
        return (By.id("com.emeint.android.myservices:id/doneBtn"));
    }

    public static By getrechagreheader(final AndroidDriver driver) {
        return (By.id("com.emeint.android.myservices:id/tv_balance_header"));
    }

    public static By getrechargeforotherheader(final AndroidDriver driver) {
        return (By.xpath("//android.widget.TextView[@text='Recharge ADSL for others']"));
    }

    public static By getreactivebundle(final AndroidDriver driver) {
        return (By.xpath("//android.widget.TextView[@text='You are out of balance']"));
    }

    public static By ADSLPaymenthistory(final AndroidDriver driver) {
        return (By.xpath("//android.widget.TextView[@text='ADSL Payment History']"));
    }


    public static By ADSLChangePlan(final AndroidDriver driver) {
        return (By.xpath("//android.widget.TextView[@text='Change ADSL Plan']"));
    }

    public static WebElement DoneButton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/doneBtn"));
    }

    public static By getActiveStatusAR(final AndroidDriver driver) {
        return (By.xpath("//android.widget.TextView[@text='فعالة']"));
    }

    public static By getSubscribenumbers(final AndroidDriver driver) {
        return (By.id("com.emeint.android.myservices:id/landlineLabel"));
    }

    public static WebElement getrechargeforothersAR(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/serviceActionBtn"));
    }

    public static By ADSLPaymenthistoryAR(final AndroidDriver driver) {
        return (By.id("com.emeint.android.myservices:id/toolbar_title"));
    }

    public static By getSuspendedStatusAR(final AndroidDriver driver) {
        return (By.xpath("//android.widget.TextView[@text='متوقفة']"));
    }

    public static By getrechargeforotherheaderAR(final AndroidDriver driver) {
        return (By.xpath("//android.widget.TextView[@text='شحن رصيد ADSL للغير']"));
    }

    public static WebElement getreactivebuttonAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='تفعيل خدمة ال ADSL']"));
    }

    public static By getreactivebundleAR(final AndroidDriver driver) {
        return (By.xpath("//android.widget.TextView[@text='لقد انتهي رصيدك']"));
    }
}
