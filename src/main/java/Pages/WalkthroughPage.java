package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WalkthroughPage {

    public static WebElement getHomepageWalkthrough(final AndroidDriver driver) {
        return driver.findElement(By.id("btnAction"));
    }

    public static WebElement getStaticWalkthrough(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/material_target_prompt_view"));
    }
}
