package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;


public class Nudge {
    ////get side_menu_elements
    public static WebElement getsidemenu(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_side_menu");

    }

    public static WebElement getOKButtonFailure(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/vb_operation");
    }

    public static WebElement getbehiendNudgeID(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Mobile Internet Management']");

    }

    public static WebElement getMIsection(final AndroidDriver driver) {

        return driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.support.v7.widget.RecyclerView[1]/android.widget.LinearLayout[3]/android.widget.TextView");
        //return driver.findElementByXPath("//android.widget.TextView[@text='Mobile internet']");

    }

    public static WebElement getBackToSideMenu(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/toolbar_menu_icon");


    }

    public static WebElement gethomeicon(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Home']");


    }


    public static WebElement getBackToHome(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/popular_icon");
    }

    public static WebElement getNudgeEntry(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/arcBg");
    }

    public static WebElement getOfferDes(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/subTitleTv");
    }

    public static WebElement getOfferTitle(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/titleTv");
    }

    public static WebElement getBackHome(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/toolbar_backIcon");
    }

    public static WebElement getBalance(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/points");
    }

    public static WebElement getinternettab(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Internet']");
    }

    public static WebElement getinternettabAR(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='إنترنت']");
    }


    public static WebElement getQuota(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvTypeValue");


    }


    public static WebElement getRedeemBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/yesBtn");
    }

    public static WebElement getConfirmBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/yesBtn");
    }


    public static WebElement backtoHome(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_side_logo");
    }


    public static WebElement getCloseBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/noBtn");
    }

    public static WebElement LogOut(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/menu_item_icon");
    }

    public static WebElement getOKForLogOut(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/vb_operation");
    }

    public static WebElement getsettingandsupport(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Settings & Support']");
    }

    public static WebElement getsetting(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Settings']");
    }

    public static WebElement getlangArabic(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/spinner_item_name");

    }

    public static WebElement getchooselang(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='عربي']");
    }


}
