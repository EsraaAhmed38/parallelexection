package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

public class AppChat {
    public static WebElement getAppChat(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Chat with us']");
    }

    public static WebElement getAppChatAR(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='تواصل معنا']");
    }

}
