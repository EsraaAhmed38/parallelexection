package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PaymentPage_EN {

    public static WebElement getBillAndPayment(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/mainParentView"));
    }

    public static WebElement getpay_bill_for_others_Ar(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='دفع فاتورة للغير']"));
    }


    public static WebElement getpay_Mybill(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My Bill ']"));
    }

    public static WebElement getPayNow(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnPayNow");
    }

    public static WebElement getaddNewCard(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnCreditCardAdd");
    }

    public static WebElement getaddCardHolder(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/etCardName");
    }

    public static WebElement getaddCardNumber(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/etCardNumber");
    }

    public static WebElement getaddCardYear(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='YY']"));
    }


    public static WebElement getaddCardYearInput(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='2023']"));
    }

    public static WebElement getaddCardMonth(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='MM']"));
    }

    public static WebElement getaddCardMonthInput(final AndroidDriver driver, String month) {
        return driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='" + month + "']"));
    }

    public static WebElement getMybill_Home(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My Bill']"));
    }

    public static WebElement getaddCardCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/etCvv");
    }

    public static WebElement getRechargeCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }


    public static WebElement getaddCardConfirmation(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnAdd");
    }

    public static WebElement getConfirmbyCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    public static WebElement getConfirmAddedCard(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='OK']"));
    }

    public static WebElement getConfirmAddedCardAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='تم']"));
    }

    public static WebElement getaddCardScript(final AndroidDriver driver) {
        //return driver.findElement(By.xpath("//android.widget.TextView[@text='Card registered successfully']"));
        return driver.findElementById("com.emeint.android.myservices:id/alertSecondaryText");
    }

    public static WebElement getaddCardScriptAr(final AndroidDriver driver) {
        //return driver.findElement(By.xpath("//android.widget.TextView[@text='تم اضافة الكارت بنجاح']"));
        return driver.findElementById("com.emeint.android.myservices:id/alertSecondaryText");

    }

    public static WebElement getConfirmFailedCard(final AndroidDriver driver) {
//		return driver.findElementById("com.emeint.android.myservices:id/vb_operation");
//		return driver.findElementById("com.emeint.android.myservices:id/iv_exit");
        return driver.findElement(By.xpath("//android.widget.LinearLayout//android.widget.Button[@text='OK']"));

    }

    public static WebElement getConfirmFailedCardAr(final AndroidDriver driver) {
//		return driver.findElementById("com.emeint.android.myservices:id/vb_operation");
//		return driver.findElementById("com.emeint.android.myservices:id/iv_exit");
        return driver.findElement(By.xpath("//android.widget.LinearLayout//android.widget.Button[@text='نعم']"));
    }

    public static WebElement getCardsListDropdown(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnCreditCard");
    }

    public static WebElement getAddCardDropdown(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_addCreditCard");
    }

    public static WebElement getpay_bill_for_others(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Pay bill for others']"));
    }

    public static WebElement getpayRechargeForOthers(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge for others']"));
    }

    public static WebElement getPaymentHistory(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Payment History']"));
    }

    public static WebElement getTopReports(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Top reports']"));
    }

    public static WebElement getUnbilledAmount(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Unbilled amount']"));
    }

    public static WebElement getDetailedConsumption(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Detailed consumption']"));
    }

    public static WebElement getManageMyCards(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tv_manageCreditCard"));
    }

    public static WebElement getdeleteCards(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/iv_deleteCard"));
    }

    public static WebElement getPaymentNow(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/btnCreditCardDone"));
    }


    //////////////////////// prepaid/////////////////////////////////////////////////////

    public static WebElement getRecharge_Home(final AndroidDriver driver) {

        //return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge']"));
        //return driver.findElementById("com.emeint.android.myservices:id/selectableRoundedImageView");
        return driver.findElementById("com.emeint.android.myservices:id/points_container");


    }
    //public static WebElement getRecharge_Home_AR(final AndroidDriver driver) {

    //return driver.findElement(By.xpath("//android.widget.TextView[@text='الرصيد']"));
    //return driver.findElementById("com.emeint.android.myservices:id/selectableRoundedImageView");

    //}

    public static WebElement getRechargeAndBalance(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge and Balance']"));

    }

    public static WebElement getRechargeAndBalance_AR(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='شحن الرصيد']"));

    }

    public static WebElement getAmountToPay(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/leftSideInput");
    }

    public static WebElement getBalanceToPay(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/rightSideInput");
    }

//	public static WebElement getBalanceincard(final AndroidDriver driver) {
//		return driver.findElementById(" com.emeint.android.myservices:id/rightSideInput");
//	}


    public static WebElement getRechargeConfrmation(final AndroidDriver driver) {

        //return driver.findElement(By.id("com.emeint.android.myservices:id/btn_confirm']"));
        return driver.findElement(By.xpath("//android.widget.Button[@text='Confirm']"));
    }


    public static WebElement getRecharge(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge']"));
    }

    public static WebElement getRechargeAR(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='شحن الرصيد']"));
    }

    public static WebElement getRechargeForOthers(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge for others']"));
    }

    public static WebElement getBalanceDetails(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Balance details']"));
    }

    public static WebElement getADDNEWCARD(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='ALI MOHAMED GAMEL HASSAN']"));
    }

    public static WebElement getmorethanCrCardsection(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Credit Card']");
    }

    public static WebElement getselectcard(final AndroidDriver driver) {

        return driver.findElementById("com.emeint.android.myservices:id/creditCardName");
    }

    public static WebElement AddcardFromDropMenu(final AndroidDriver driver) {

        // return
        // driver.findElementById("com.emeint.android.myservices:id/header");
        return driver.findElementById("com.emeint.android.myservices:id/btnCreditCard");
    }

    public static WebElement getscratchCard(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Scratch Card']");
    }

    public static WebElement getvodafoneCash(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Vodafone Cash']");
    }

    public static WebElement getconfirmBTN(final AndroidDriver driver) {
        // return
        // driver.findElementByXPath("//android.widget.TextView[@text='Confirm']");
        return driver.findElementById("com.emeint.android.myservices:id/alertButtonsLayout");
    }

    public static WebElement getCardsList(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/rv_CreditCard");
    }
    //////////////////////////////////////////////////////// try

    public static WebElement getCrCardspecificExpMonthField(final AndroidDriver driver, String month) {
        return driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='" + month + "']"));
    }

    public static WebElement getCrCardspecificExpYearField(final AndroidDriver driver, String year) {
        return driver.findElementByXPath("//android.widget.CheckedTextView[@text='" + year + "']");
    }

    public static WebElement RechageForOthers(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Recharge for others']");
    }

    public static WebElement RechageForOthers_Ar(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='شحن رصيد للغير']");
    }

    //-----------------------------------------------------------------------------------------------------------
    public static WebElement getaddCardMonthInput(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='12']"));
    }

    public static WebElement getManageMyCardsPage(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnManageCreditCards"));
    }

    public static WebElement ConfirmDelBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("android:id/button1"));
    }

    public static WebElement getMobileNumber(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/et_mobile_number"));
    }

    public static WebElement getAmount(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/et_amount"));
    }

    public static WebElement getBackBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/toolbar_backIcon");
    }

    public static WebElement getBillLimitBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/scBillLimit");
    }

    public static WebElement getScratchCardNumber(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_scratch_card");
    }

    public static WebElement getSnackBar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/snackbar_text");
    }

    public static WebElement getRechargeforOthersBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/serviceActionBtn");
    }

    public static WebElement getAddUsbBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/add_usb_btn");
    }

    public static WebElement getUsbMobile_no(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/mobile_editText");
    }

    public static WebElement getUsbSIM_no(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/usb_serial_number_editText");
    }

    public static WebElement getUsbName(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/usb_name_editText");
    }

    public static WebElement getUsbAddNew_NumBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/bind_usb_save_btn");
    }

    public static WebElement OpenAddedUsbNum(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/usb_number");
    }

    public static WebElement goToRechargeUsb(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Recharge']");

    }

    public static WebElement PayPartialAmount(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/rd_payAmount");
    }

    public static WebElement getNoPayPartialamount(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnActionTwo");
    }
}
