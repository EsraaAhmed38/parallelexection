package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Roaming {


    //Roaming New Home Page
    public static WebElement getCountrySearchHome(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Please choose your destination']"));
    }

    public static WebElement getCountrySearchScriptHome(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Please choose your destination']"));
    }

    public static WebElement getKalemnyShokranScripts(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Stay in touch with your loved ones with Kalemny Shokran service']"));
    }
	
	/*public static WebElement getKalemnyShokranButton(final AndroidDriver driver) {

		return driver.findElement(By.xpath("//android.widget.TextView[@text='View more']"));
	}*/

    public static WebElement getRechargeScript(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge while you are travelling']"));
    }

    public static WebElement getTipsScript(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='View important tips for roaming']"));
    }

    //All Countries Page
    public static WebElement getCountrySearchPage(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/etSearch"));
    }

    public static WebElement getCountrySearchPageScript(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Please type your destination']"));
    }


    public static WebElement getFranceFlag(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/ivCountryFlag"));
    }

    public static WebElement getFranceButton(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='France']"));
    }

    //Bundles Page Details in France
    public static WebElement getFranceinBundlesPage(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/llCountry"));
    }

    public static WebElement getMonthyTab(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Monthly']"));
    }

    public static WebElement getWeeklyTab(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Weekly']"));
    }

    public static WebElement getDailyTab(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Daily']"));
    }

    public static WebElement getNoBundlesAvailable(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='No bundles available']"));
    }

    //Pay as You Go Banner & Page Details
    public static WebElement getPayasYouGoButton(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Pay as you go']"));
    }

    public static WebElement getPayasYouGoScript(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Roam freely with no planned bundles']"));
    }

    public static WebElement getPayasYouGoPageScript(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvPayAsYouGo"));
    }

    //Data Weekly Bundles in France
    public static WebElement getDataWeeklyBundlesText(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Data bundles']"));
    }

    public static WebElement getDataWeeklyBundleName(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Weekly 150LE Data Bundle']"));
    }

    public static WebElement getDataWeeklyBundlePrice(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='150 EGP']"));
    }

    public static WebElement getDataWeeklyBundleValidty(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Valid for 7 days']"));
    }

    public static WebElement getDataWeeklyBundleDetails(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='300 MB']"));
    }

    public static WebElement getSubscribeButton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnSubscribe"));
    }

    //Weekly All In One Bundles in France
    public static WebElement getAIOWeeklyBundleText(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='All in one bundles']"));
    }

    public static WebElement getAIOWeeklyBundleName(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Weekly 300LE Bundle']"));
    }

    public static WebElement getAIOWeeklyBundlePrice(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='300 EGP']"));
    }

    public static WebElement getAIOWeeklyBundleValidty(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Valid for 7 days']"));
    }

    public static WebElement getAIOWeeklyBundleOutMins(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='10 Outgoing mins']"));
    }

    public static WebElement getAIOWeeklyBundleInMins(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='60 Incoming mins']"));
    }

    public static WebElement getAIOWeeklyBundleSMS(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='60 SMS']"));
    }

    public static WebElement getAIOWeeklyBundleData(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='500 MB']"));
    }

    //Weekly 150 Data bundle Subscription Overlay
    public static WebElement getSubscriptionHeader(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Choose your bundle type']"));
    }

    public static WebElement getSubscriptionBundleName(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Weekly 150LE Data Bundle']"));
    }

    public static WebElement getSubscriptionBundlePrice(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='150 EGP']"));
    }

    public static WebElement getSubscriptionBundleValidty(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Valid for 7 days']"));
    }

    public static WebElement getSubscriptionBundleDetails(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='300 MB']"));
    }

    public static WebElement getSimilarCountries(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/llCountriesWorkWithThisBundle"));
    }

    public static WebElement getOneTimeOnly(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/rbOneTimeOnly"));
    }

    public static WebElement getRenewable(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/rbRenewable"));
    }

    public static WebElement getTotalBundleScript(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvTotalBundleLabel"));
    }

    public static WebElement getTotalBundlePrice(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvTotalBundle"));
    }

    public static WebElement getPerWeek(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvPerWeekLabel"));
    }

    public static WebElement getTaxesScriptOneTimeOnly(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='This is the bundle price without taxes. Your bundle will be deducted and activated upon first usage.']"));
    }

    public static WebElement getTaxesScriptRenewable(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='This is the bundle price without taxes. Your bundle will be deducted and activated upon first usage and will be renewed automatically.']"));
    }

    public static WebElement getConfirmationButton(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/btnConfirm"));
    }

    //Subscription Confirmation Overlay
    public static WebElement getConfirmationOverlayIcon(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/iconIv"));
    }

    public static WebElement getConfirmationOverlayScript(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Your bundle will be automatically activated upon first usage at destination.']"));
    }

    public static WebElement getConfirmationOverlayDoneBtn(final AndroidDriver driver) {

        return driver.findElement(By.xpath("com.emeint.android.myservices:id/doneBtn"));
    }

    //Daily Data Bundles in France
    public static WebElement getDataDailyBundlesText(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Data bundles']"));
    }

    public static WebElement getDataDailyBundleName(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Daily 30LE Data Bundle']"));
    }

    public static WebElement getDataDailyBundlePrice(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='30 EGP']"));
    }

    public static WebElement getDataDailyBundleValidty(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Valid for 1 day']"));
    }

    public static WebElement getDataDailyBundleDetails(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='30 MB']"));
    }


    //Daily All In One Bundles in France
    public static WebElement getAIODailyBundleText(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='All in one bundles']"));
    }

    public static WebElement getAIODailyBundleName(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Daily 50LE Bundle']"));
    }

    public static WebElement getAIODailyBundlePrice(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='50 EGP']"));
    }

    public static WebElement getAIODailyBundleValidty(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Valid for 1 day']"));
    }

    public static WebElement getAIODailyBundleOutMins(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='5 Outgoing mins']"));
    }

    public static WebElement getAIODailyBundleInMins(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='5 Incoming mins']"));
    }

    public static WebElement getAIODailyBundleSMS(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='25 SMS']"));
    }

    public static WebElement getAIODailyBundleData(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='25 MB']"));
    }

    //Daily 50 AIO Bundle Subscription Overlay
    public static WebElement getSubscriptionHeaderDaily(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Choose your bundle type']"));
    }

    public static WebElement getSubscriptionDailyBundleName(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Daily 50LE Bundle']"));
    }

    public static WebElement getSubscriptionDailyBundlePrice(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='50 EGP']"));
    }

    public static WebElement getSubscriptionDailyBundleValidty(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Valid for 1 day']"));
    }

    public static WebElement getSubscriptionDailyBundleOutMin(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='5 Outgoing mins']"));
    }

    public static WebElement getSubscriptionDailyBundleInMin(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='5 Incoming mins']"));
    }

    public static WebElement getSubscriptionDailyBundleSMS(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='25 SMS']"));
    }

    public static WebElement getSubscriptionDailyBundleData(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='25 MB']"));
    }

}
