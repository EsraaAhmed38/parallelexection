package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;


public class Whatsapp {

    public static WebElement getFlex(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/serviceData");
    }

    public static WebElement getFlexManagement(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/serviceData");
    }

    public static WebElement getEnglishScript(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='With Flex bundles you will enjoy free WhatsApp MBs every month, or you can use WhatsApp from your main bundle by turning off WhatsApp option.']");
    }

    public static WebElement getArabicScript(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='مع باقات فليكس هتستمتع بـ ميجابايتس ببلاش للواتساب كل شهر أو تقدر في أي وقت تستخدم الواتساب من باقتك الأساسية عن طريق اختيار ايقاف الواتساب.']");
    }

    public static WebElement getToogleBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/switch_speed");
    }

    public static WebElement getConfirmationOverlay(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_title_text");
    }

    public static WebElement getOnScript(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_primary_text");
    }

    public static WebElement getCloseBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_primary_text");
    }

    public static WebElement getConfrimBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/vb_operation");
    }

    public static WebElement getSuccessScript(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/snackbar_text");
    }

    public static WebElement getLabel(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/label");
    }

    public static WebElement getData(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/data");
    }

    public static WebElement getDesc(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/desc");
    }


}
