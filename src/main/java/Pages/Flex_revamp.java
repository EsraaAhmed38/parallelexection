package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;


public class Flex_revamp {
    public static WebElement getFlexManage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/serviceData");
    }

    public static WebElement getRepurchaseBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_repurchase");
    }

    public static WebElement getFlexExtra(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Flex Extra']");
    }

    public static WebElement getSuperFlex(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Super Flex']");
    }

    public static WebElement getMILimit(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/internetLimitCard");
    }

    public static WebElement getCrossnetLimit(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/minutesLimitCard");
    }

    public static WebElement getWhatsapp(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Free WhatsApp']");
    }

    public static WebElement getFlexTransfer(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Flex Transfer']");
    }

    public static WebElement getFlex365(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='365 Offers']");
    }

    public static WebElement getAutoRenew(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Auto bundle renewal']");
    }

    public static WebElement getRollOver(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Flex Rollover']");
    }

}
