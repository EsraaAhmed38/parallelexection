package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;


public class Sidemenu_AR {
////get side_menu_elements


    public static WebElement getClose(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/close_btn");
    }

    public static WebElement getBack(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/toolbar_backIcon");
    }

    public static WebElement getHome(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='الصفحة الرئيسية']");
    }

    public static WebElement getHomePage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/fab");
    }

    public static WebElement getMyPlan(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='نظامي']");
    }

    public static WebElement getMyPlanPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='نظام حسابي']");
    }

    public static WebElement getHomeSidemenuId(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iwt_menu");
    }

    public static WebElement getSidemenu(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/toolbar_menu_icon");
    }

    public static WebElement getMobileInternet(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='الموبايل إنترنت']");
    }

    public static WebElement getMobileInternetPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='إدارة الموبايل انترنت']");
    }

    public static WebElement getFlex365(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='فليكس 365']");
    }

    public static WebElement get365offersPage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/flex_365_title");
    }

    public static WebElement getSallefnyShokran(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='سلفني شكرا']");

    }

    public static WebElement getSallefnyShokranPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='سلفني شكرا']");

    }

    public static WebElement getBalanceTransfer(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='تحويل رصيد']");
    }

    public static WebElement getBalanceTransferPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='تحويل رصيد']");
    }

    public static WebElement getRechargeandBalace(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='شحن و متابعه رصيد ']");
    }

    public static WebElement getManageFlex(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='إدارة فليكساتي']");
    }

    public static WebElement getFlexManagementPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='إداره الفليكسات']");
    }

    //not exist in flex
    public static WebElement get365Offers(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='365 Offers']");
    }

    public static WebElement getTeam010(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='فريق 010 ']");
    }

    public static WebElement getTeam010Page(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvTitle");
    }

    public static WebElement getVodafonecash(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='فودافون كاش']");
    }


    public static WebElement getADSL(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='DSL  ']");
    }

    public static WebElement getServices(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='خدمات']");
    }

    public static WebElement getUSB(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='USB']");
    }

    public static WebElement getUSBPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='ال USB الخاص بي']");
    }

    public static WebElement getWhitelist(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='القائمة البيضاء']");
    }

    public static WebElement getWhitelistPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='القائمة البيضاء']");
    }

    public static WebElement getBlacklist(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='القائمة السوداء']");
    }

    public static WebElement getBlacklistPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='القائمة السوداء']");
    }

    public static WebElement getMissedCallKeeper(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='الإحتفاظ بالمكالمات']");
    }

    public static WebElement getMyRedFamily(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='My Red Family']");
    }

    public static WebElement getMyRedFamilyPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='أنا ڤودافون']");
    }

    public static WebElement get3ala7esaby(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='على حسابي']");
    }

    public static WebElement getRoaming(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='التجوال']");
    }

    public static WebElement getEdfa3ly(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='ادفعلي شكرا']");
    }

    public static WebElement getEdfa3lyPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='ادفعلي شكرا']");
    }

    public static WebElement getKallemny(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='كلمني شكرا']");
    }

    public static WebElement getShop(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='تسوّق']");
    }

    public static WebElement getStore(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='فروع فودافون']");
    }

    public static WebElement getSIM(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='استبدال الشريحة']");
    }

    public static WebElement getSIMPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='تفعيل الشريحة']");
    }

    public static WebElement getEntertainment(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='ترفيه']");
    }

    public static WebElement getVodafone_Fantasy(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Vodafone Fantasy']");
    }

    public static WebElement getAlerting_services(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text=' الرسايل الاخبارية']");
    }

    public static WebElement getPlayWin(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='إلعب واكسب']");
    }

    public static WebElement getMohamedSalah(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='عالم محمد صلاح']");
    }

    public static WebElement getMohamedSalahPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.view.View[@content-desc='عالم محمد  صلاح']");
    }

    public static WebElement getModarag_Vodafone(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='مدرج فودافون']");
    }

    public static WebElement getVodafoneTv(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text=' TV فودافون ']");

    }

    public static WebElement getVodafoneTvPage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/webView");

    }

    public static WebElement getSettings_Support(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='الاعدادت و الضبط']");

    }

    public static WebElement getSettings(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='الضبط']");

    }

    public static WebElement getSettingsPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='اللغة']");

    }

    public static WebElement getMyProfile(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='بياناتي']");

    }

    public static WebElement getMyProfilePage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='النوع']");

    }

    ///////////////////////////RED////////////////////////////////////////////////////
    public static WebElement getMyBill(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='فاتورتي']");
    }

    public static WebElement getMyBillPage(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='إدارة الفاتورة']");
    }

    public static WebElement getBill_Payments(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='الفواتير/المدفوعات']");
    }

    public static WebElement getRedServices(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='خدمات Red']");
    }

}
