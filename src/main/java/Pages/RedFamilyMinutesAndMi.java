package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RedFamilyMinutesAndMi {

    public static WebElement getFamilyMinutes(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Family minutes']"));

    }

    public static WebElement getFamilyData(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Family internet']"));

    }

    public static WebElement getFamilySms(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Family SMS']"));

    }

    public static WebElement getFamilyMinutesAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='دقائق العيلة']"));

    }

    public static WebElement getFamilyDataAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='ميجابيتس العيلة']"));

    }

    public static WebElement getFamilySmsAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='رسائل العيلة']"));

    }

}



