package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SplashScreen {


    public static WebElement getSplashScreen(final AndroidDriver driver) {

        // return driver.findElement(By.id("com.huawei.android.launcher:id/preview_background"));
        return driver.findElement(By.id("com.emeint.android.myservices:id/splash_animating_bg_imageview"));


    }


}
