package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class Cancel_Popup_login {

    public static WebElement getcancelButton(final AndroidDriver driver) {
        driver.findElement(By.id("com.emeint.android.myservices:id/etOnboarding")).click();
        return driver.findElement(By.id("com.google.android.gms:id/cancel"));
    }

}


