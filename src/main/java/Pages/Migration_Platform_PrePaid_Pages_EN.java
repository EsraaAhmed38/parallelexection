package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Migration_Platform_PrePaid_Pages_EN {

    static String SidemenuXPath = "com.emeint.android.myservices:id/iwt_menu";

    //side menu
    public static WebElement getsidemenu(final AndroidDriver driver) {
        return driver.findElementById(SidemenuXPath);  //DONE

    }

    public static WebElement getOpsButton(final AndroidDriver driver) {
        return driver.findElementById("om.emeint.android.myservices:id/doneBtn");  //

    }

    public static WebElement getMyPlanTap(final AndroidDriver driver) {
        return (WebElement) driver.findElements(By.id("com.emeint.android.myservices:id/popular_cell")).get(1);  //DONE
    }

    public static WebElement getExplorebutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnExploreMorePlans");  //DONE
//           return driver.findElementByXPath("//android.widget.Button[text() , 'Explore']");
        //return driver.findElementByXPath("//android.widget.Button[@index='2']");
    }

    public static WebElement getUPgrade(final AndroidDriver driver) {
        //return driver.findElementById("com.emeint.android.myservices:id/vfbtnUpgrade");    //DONE
        return (WebElement) driver.findElements(By.id("com.emeint.android.myservices:id/vfbtnUpgrade")).get(0);

    }

    public static WebElement getUPgradeconfirm(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");     //DONE
    }

    public static WebElement getUPgradelater(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnSecondary");   //DONE
    }

    public static WebElement sucesstxt(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/titleTv");    //
    }

    public static WebElement donebutton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");   //DONE
    }

    public static WebElement sucessscriptdetails(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/messageTv");  //DONE
    }
}