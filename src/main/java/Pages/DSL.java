package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DSL {

    // Subscription Page

    public static WebElement getSubscribeBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnSubscribeNow");
    }

    public static WebElement getFirstName(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='First Name']"));

    }

    public static WebElement getFirstNameAr(final AndroidDriver driver) {
        //return driver.findElement(By.xpath("//android.widget.EditText[@text='الاسم الأول']"));
        return driver.findElementById("com.emeint.android.myservices:id/eTxtFirstName");

    }

    public static WebElement getLastName(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='Family Name']"));
    }

    public static WebElement getLastNameAr(final AndroidDriver driver) {
        //return driver.findElement(By.xpath("//android.widget.EditText[@text='اسم العائلة']"));
        return driver.findElementById("com.emeint.android.myservices:id/eTxtLastName");

    }

    public static WebElement getCityCode(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='City code']"));
    }

    public static WebElement getCityCodeAr(final AndroidDriver driver) {
        // return driver.findElementById("com.emeint.android.myservices:id/cityCodeContainer") ;
        return driver.findElement(By.xpath("//android.widget.EditText[@text='كود المحافظة']"));
    }

    public static WebElement getLandLine(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='Landline number']"));
    }

    public static WebElement getLandLineAr(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/eTxtLandLineNo");

        //return driver.findElement(By.xpath("//android.widget.EditText[@text=' رقم التيلفون الأرضي']"));
    }

    public static WebElement getMSISDN(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='Vodafone number']"));
    }

    public static WebElement getMSISDNAr(final AndroidDriver driver) {
        //return driver.findElementById("com.emeint.android.myservices:id/eTxtMsisdn") ;
        return driver.findElement(By.xpath("//android.widget.EditText[@text='رقم ڤودافون']"));
    }

    public static WebElement getAnotherMSISDN(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='Another preferred contact number']"));
    }

    public static WebElement getAnotherMSISDNAr(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/eTxtPreferedNo");
        //return driver.findElement(By.xpath("//android.widget.EditText[@text=' رقم آخر للتواصل']"));
    }

    public static WebElement getAnotherMSISDNCheckBox(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.CheckBox[@text='Do you have another preferred contact number?']"));
    }

    public static WebElement getAnotherMSISDNCheckBoxAr(final AndroidDriver driver) {

        return driver.findElementById("com.emeint.android.myservices:id/preferedCheckMark");
        //return driver.findElement(By.xpath("//android.widget.CheckBox[@text='هل لديك رقم أخر للتواصل؟']"));
    }

    public static WebElement getSubmitBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/adslRegisterBtn");

    }

    public static WebElement getSubmitBtnAr(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/adslRegisterBtn");
        //return driver.findElement(By.xpath("//android.widget.Button[@text='تقديم الطلب']"));

    }

    // Confirmation Page

    public static WebElement getConfirmBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/almostDoneConfirmBtn");
    }

    public static WebElement getActivationKey(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/eTxtActivationKey");
    }

    public static WebElement getExitBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_exitn");
    }

    // Successful Request

    public static WebElement getConfirmationMessage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/error_msg_text_view");
    }

    public static WebElement getOKBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/adslSuccessBtn");
    }

    public static WebElement getRequestNumber(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvRequestNoValue");
    }

    public static WebElement getCheckPrevRequest(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='Check your ADSL request']"));
    }
    // Add More Quota

    public static WebElement getAddQuotaBtn(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/getMoreAddonsBtn");
    }

    public static WebElement getBuyAddonBtn(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='Buy this Add-on']"));
    }

    public static WebElement getOnDemandAddonBtn(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='On demand']"));
    }

    public static WebElement getOnDemandAddonBtnAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='حسب الطلب']"));
    }

    public static WebElement getBuyAddonBtnAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='اشتري الآن']"));
    }

    public static WebElement getDoneBtn(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='Done']"));
    }

    public static WebElement getDoneBtnAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='تم']"));
    }

    public static WebElement getRenewableAddon(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Renewable']"));
    }

    public static WebElement getRenewableAddonAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='متجددة']"));
    }

    // Survey

    public static WebElement getSurvey(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/dialog_survey_desc");
    }

    public static WebElement getCancelSurvey(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/survey_cancel_btn");
    }

    // Error in Loading Quota
    public static WebElement getTryAgain(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tryLoadingQuota");
    }

    public static WebElement getErrorMessage(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/errorLoadingQuotaInfo");
    }


    ////DSL status

    public static WebElement getActivestatus(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/getMoreAddonsBtn");
    }

    public static WebElement getActivestatusAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='فعالة']"));
    }

    public static WebElement getSuspendedstatus(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Suspended']"));
    }

    public static WebElement getSuspendedstatusAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='متوقفة']"));
    }

    public static WebElement getReactiveBtn(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='Reactivate your ADSL']"));
    }

    public static WebElement getReactiveBtnAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='تفعيل خدمة ال ADSL']"));
    }

    public static WebElement getRechargeDslMessage(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/doneBtn"));
    }

    public static WebElement getConfirmAddonOndemand(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/doneBtn"));
    }

    public static WebElement getConfirmAddonTxt(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Add-on added successfully']"));
    }

    public static WebElement getRechargeAddonBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/doneBtn"));
    }

    ///////////////////////////Payment //////////////////////////////////////////
    public static WebElement getscratchcard(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Scratch Card']");
    }

    public static WebElement getScratchCardNumber(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_scratch_card");
    }

    public static WebElement getrechargeBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/done_btn");
    }

    public static WebElement getSnackBar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/snackbar_text");

    }

    /////////////////////////DSL For Cc
    public static WebElement getAmountField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/amountContainer");
    }

    public static WebElement getCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }


    public static WebElement getCrCardSaved(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnCreditCard");
    }

    public static WebElement getdesiredsavedCrCard(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/info");
    }

    public static WebElement getConfirmbyCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    public static WebElement getOKBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    //payment for others
    public static WebElement getRechargeforOthersBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/serviceActionBtn");
    }

		/*public static WebElement getLandLine(final AndroidDriver driver) {
		return driver.findElementById("com.emeint.android.myservices:id/et_mobile_number");
		}*/

    public static WebElement getContactsBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/contacts_btn");
    }

    public static WebElement getAllowContactsBTN(final AndroidDriver driver) {
        return driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
    }

    public static WebElement getSearchField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/searchET");
    }

    public static WebElement getSelectedLandLinefield(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/selectedCB");
    }

    public static WebElement getSelectedLandLineBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/selectContact");
    }

    ////////CC for self


    public static WebElement getfailOKBTN(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.Button[@text='OK']");
    }

    public static WebElement getAddCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }


    public static WebElement getDeleteCardicon(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_deleteCard");
    }

    public static WebElement getDeleteconfirmationoption(final AndroidDriver driver) {
        return driver.findElementById("android:id/button1");
    }

    //find "no" option from confirmation delete
    public static WebElement getNotDeleteconfirmationoption(final AndroidDriver driver) {
        return driver.findElementById("android:id/button2");
    }

    //Get Recharge BTN once click on ADSL from side menu
    public static WebElement getADSLRechageBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/walletRechargeBtn");
    }

    public static WebElement getErrorMsgforwrongCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/alertSecondaryText");
    }

    public static WebElement getAddNewCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/addCreditCardBtn");
    }

    public static WebElement getCrCardnameField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_card_name");
    }

    public static WebElement getCrCardNumberField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_card_number");
    }

    public static WebElement getCrCardExpYearField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='YY']");
    }

    public static WebElement getCrCardspecificExpYearField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.CheckedTextView[@text='2021']");
    }

    public static WebElement getCrCardspecificExpMonthField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.CheckedTextView[@text='09']");
    }

    public static WebElement getCrCardExpMonthField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='MM']");
    }

    public static WebElement getCrCardCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }

    public static WebElement getAddThisCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_add");
    }

    public static WebElement getErrorMsgfornotaddedCrCard(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Sorry! Your transaction failed, please try again or contact your bank for more assistance']");
    }

    public static WebElement getCongratulationstext(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Congratulations']");
    }

    public static WebElement getCardregisteredsuccessfullytext(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Card registered successfully']");
    }

    //Receipt info
    public static WebElement getCardReceiptdoneBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_print");
    }

    public static WebElement getCardReceiptlandline(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_mobile_number");
    }

    public static WebElement getCardReceiptdate(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_date");
    }

    public static WebElement getCardReceiptsuccessmsg(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/titleDesc");
    }

    public static WebElement getCardReceiptamount(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_bill_amount");
    }


}
