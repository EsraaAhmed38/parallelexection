package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EditProfile_Page {

//	MSISDN = Utilities.ExcelUtils.getCellData(51, 0);// 01010467446

    public static WebElement getPFirstName(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/first_name_editText"));
    }

    public static WebElement getLastName(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/last_name_editText"));
    }

    public static WebElement getGenderMenu(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/gender_spinner"));
    }

    public static WebElement getMaleGender(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Male']");
    }

    public static WebElement getMaleGenderAr(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='ذكر']");
    }

    public static WebElement getFemaleGender(final AndroidDriver driver) {
        //       return driver.findElementByXPath("//android.widget.TextView[@text='Female']");
//        return driver.findElementByXPath("//android.widget.ListView/android.widget.TextView[@text='Female']")
//        return (WebElement) driver.findElementsById("android:id/text1").get(1);
//        driver.findElementByClassName("android.widget.TextView");
        return driver.findElementByAndroidUIAutomator("text(\"Female\")");
//         List<MobileElement> els1 = (List<MobileElement>) driver.findElementsById("Female");
//         return (WebElement) els1;

    }

    public static WebElement getFemaleGenderAr(final AndroidDriver driver) {
//        return driver.findElementByXPath("/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[@text='أنثى']");
        return driver.findElementByName("أنثى");

    }

    public static WebElement getCitiesButton(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/cities_spinner");
    }

    public static WebElement getCitiesMenu(final AndroidDriver driver, String city) {
        return driver.findElementByXPath("//android.widget.TextView[@text='" + city + "']");
    }

    public static WebElement getEmailText(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/email_editText"));
    }

    public static WebElement getMailNotificationStatus(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/email_contact_checkBox"));
    }

    public static WebElement getSaveIcon(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/action_save"));
    }

    public static WebElement getBackButton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/toolbar_backIcon"));
    }

    public static WebElement getBirthdatetext(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/date_textView"));
    }
}




