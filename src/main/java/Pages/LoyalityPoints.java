package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoyalityPoints {

    public static WebElement getLoyalitypointspageFromHome(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/titleTv"));
    }

    public static WebElement getRedservices(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Red services']"));
    }

    public static WebElement getRedservicesAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='خدمات Red']"));
    }

    public static WebElement getLoyalitypointspageFromSidemenu(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Red points']"));
    }

    public static WebElement getLoyalitypointspageFromSidemenuAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='نقاط Red']"));
    }

    public static WebElement getPointsFromHome(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/pointsTV"));
    }

    public static WebElement getAboutToexpire(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvAboutToExpire"));
    }

    public static WebElement getRecentlyUsed(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvUsed"));
    }

    public static WebElement getRecentlyExpired(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvExpired"));
    }

    public static WebElement getNumberofPoints(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvNumOfPoints"));
    }

    public static WebElement getNoPointsDisplayed(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvNoPoints"));
    }

    public static WebElement getValiddate(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvValidity"));
    }

    public static WebElement getPointsBalance(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvPointsBalance"));
    }

    public static WebElement getMoneyBalance(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvMoneyBalance"));
    }

    public static WebElement getLogosofRedDeals(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/ivLogo"));
    }

    public static WebElement getMoreDetails(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvMoreDetails"));
    }

    public static WebElement getMoreDetailsAfterRedeem(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvMoreDetailsSuccess"));
    }

    public static WebElement getGetItNowBtn(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/btnGetItNow"));
    }

    public static WebElement getRedPartnersSection(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='You can redeem your points with']"));
    }

    public static WebElement getRedPartnersSectionAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='يمكنك استخدام نقاطك مع']"));
    }

    public static WebElement getDiscountValue(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvDiscountValue"));
    }

    public static WebElement getValidityText(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Valid till the end of the month']"));
    }

    public static WebElement getValidityTextAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='صالح حتى نهاية الشهر']"));
    }

    public static WebElement getLogosInsideMoreDetails(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/ivImage"));
    }

    public static WebElement getDealsDetails(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvDetails"));
    }

    public static WebElement getTitleInsideMoreDetails(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Red Deals']"));
    }

    public static WebElement getTitleInsideMoreDetailsAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='ديلز Red']"));
    }

    public static WebElement getValidtyInsideMoreDetails(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Valid till the end of the month']"));
    }

    public static WebElement getValidtyInsideMoreDetailsAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='صالح حتى نهاية الشهر']"));
    }

    public static WebElement getBackIcon(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/toolbar_backIcon"));
    }

    public static WebElement getVoucherCode(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvVoucherCode"));
    }

    public static WebElement getRedDealsTxtBeforeLogos(final AndroidDriver driver) {

        return driver.findElement(
                By.xpath("//android.widget.TextView[@text='Visit any of the branches below and get your discount']"));
    }

    public static WebElement getRedDealsTxtBeforeLogosAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath(
                "//android.widget.TextView[@text='اختار القسم المفضل لك او ممكن البحث عن فرع معين حتى يمكنك استخدام نقاطك بكل سهولة']"));
    }

    public static WebElement getLastUpdatedTxt(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvLastUpdated"));
    }

    public static WebElement getPartnersCategoryTitle(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Partner Categories']"));
    }

    public static WebElement getPartnersCategoryTitleAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='قائمة شركاء نقاط Red']"));
    }

    public static WebElement getPartnersCategoryScript(final AndroidDriver driver) {

        return driver.findElement(By.xpath(
                "//android.widget.TextView[@text='Choose your preferred category or search for a specific store, to redeem your points easily']"));
    }

    public static WebElement getPartnersCategoryScriptAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath(
                "//android.widget.TextView[@text='اختار القسم المفضل لك او ممكن البحث عن فرع معين حتى يمكنك استخدام نقاطك بكل سهولة']"));
    }

    public static WebElement getPartnersCategory(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/spinnerCategory"));
    }

    public static WebElement getSearchBtn(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/cvSearch"));
    }

    public static WebElement getFashion(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Fashion']"));
    }

    public static WebElement getHealthAndBeauty(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Health & Beauty']"));
    }

    public static WebElement getMedical(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Medical']"));
    }

    public static WebElement getFAndB(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='F&B']"));
    }

    public static WebElement getElectronics(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Electronics']"));
    }

    public static WebElement getKidsAndToys(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Kids & Toys']"));
    }

    public static WebElement getGroceries(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Groceries']"));
    }

    public static WebElement getHotelsAndTravel(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Hotels & Travel']"));
    }

    public static WebElement getFurnitureAndHomeAccessories(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Furniture & Home Accessories']"));
    }

    public static WebElement getGiftShop(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Gift Shop']"));
    }

    public static WebElement getStationary(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Stationary']"));
    }

    public static WebElement getOptics(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Optics']"));
    }

    public static WebElement getOthers(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Others']"));
    }

    public static WebElement getSearchTxtField(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/etSearch"));
    }

    public static WebElement getLordOfTheWings(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Lord Of The Wings']"));
    }

    public static WebElement getLordOfTheWingsFirstBranch(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Cairo Festival City, New Cairo']"));
    }

    public static WebElement getNearestStoreTitle(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Choose your nearest store']"));
    }

    public static WebElement getNearestStoreTitleAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='اختار الفرع الأقرب لك']"));
    }

    public static WebElement getNearestStoreScript(final AndroidDriver driver) {

        return driver.findElement(By.xpath(
                "//android.widget.TextView[@text='Get directions to the nearest store and redeem your points easily upon purchase']"));
    }

    public static WebElement getNearestStoreScriptAr(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='حدد مكان اقرب فرع لك لاستخدام نقاطك بكل سهوله عند الشراء']"));
    }

    public static WebElement getPartnerName(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/tvPartnerName"));
    }

    public static WebElement getBranchesCard(final AndroidDriver driver) {

        return driver.findElement(By.id("com.emeint.android.myservices:id/rvPartnerBranches"));
    }

    public static WebElement getGoogleDetailedAddress(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.widget.TextView[@text='Cairo Festival City, New Cairo']"));
    }

    public static WebElement getGoogleAddress(final AndroidDriver driver) {

        return driver.findElement(By.id("com.google.android.apps.maps:id/title"));
    }

    public static WebElement getDirectionsButton(final AndroidDriver driver) {

        return driver.findElement(By.xpath("//android.support.design.chip.Chip[@text='Directions']"));
    }
}
