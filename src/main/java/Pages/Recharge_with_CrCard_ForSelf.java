package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

public class Recharge_with_CrCard_ForSelf {
    public static WebElement getAmountField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/amountContainer");
    }

    public static WebElement getCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }

    public static WebElement getrechargeBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/doneBtn");
    }


    //open dropdown menu of saved Cr Cards
    public static WebElement getCrCardSaved(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnCreditCard");
    }

    // choose one of saved Cr Cards
    public static WebElement getdesiredsavedCrCard(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/info");
    }

    public static WebElement getConfirmbyCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    public static WebElement getOKBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    public static WebElement getfailOKBTN(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.Button[@text='OK']");
    }

    public static WebElement getAddCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    public static WebElement getManageMyCards(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_manageCreditCard");
    }

    public static WebElement getDeleteCardicon(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_deleteCard");
    }

    public static WebElement getDeleteconfirmationoption(final AndroidDriver driver) {
        return driver.findElementById("android:id/button1");
    }

    // find "no" option from confirmation delete
    public static WebElement getNotDeleteconfirmationoption(final AndroidDriver driver) {
        return driver.findElementById("android:id/button2");
    }

    public static WebElement getSnackBar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/snackbar_text");
    }

    //Get Recharge BTN once click on ADSL from side menu
    public static WebElement getADSLRechageBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/walletRechargeBtn");
    }

    public static WebElement getErrorMsgforwrongCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/alertSecondaryText");
    }

    public static WebElement getAddNewCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/addCreditCardBtn");
    }

    public static WebElement getCrCardnameField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_card_name");
    }

    public static WebElement getCrCardNumberField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_card_number");
    }

    public static WebElement getCrCardExpYearField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='YY']");
    }

    public static WebElement getCrCardspecificExpYearField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.CheckedTextView[@text='2021']");
    }

    public static WebElement getCrCardspecificExpMonthField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.CheckedTextView[@text='09']");
    }

    public static WebElement getCrCardExpMonthField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='MM']");
    }

    public static WebElement getCrCardCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }

    public static WebElement getAddThisCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_add");
    }

    public static WebElement getErrorMsgfornotaddedCrCard(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Sorry! Your transaction failed, please try again or contact your bank for more assistance']");
    }

    public static WebElement getCongratulationstext(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Congratulations']");
    }

    public static WebElement getCardregisteredsuccessfullytext(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Card registered successfully']");
    }

    //Receipt info
    public static WebElement getCardReceiptdoneBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_print");
    }

    public static WebElement getCardReceiptlandline(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_mobile_number");
    }

    public static WebElement getCardReceiptdate(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_date");
    }

    public static WebElement getCardReceiptsuccessmsg(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/titleDesc");
    }

    public static WebElement getCardReceiptamount(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_bill_amount");
    }


}
