package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

public class Payment_History_ADSL {
    public static WebElement getAmountField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/amountContainer");
    }

    public static WebElement getCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }

    public static WebElement getrechargeBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/done_btn");
    }


    //open dropdown menu of saved Cr Cards
    public static WebElement getCrCardSaved(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btnCreditCard");
    }

    // choose one of saved Cr Cards
    public static WebElement getdesiredsavedCrCard(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/info");
    }

    public static WebElement getConfirmbyCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    public static WebElement getOKBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    public static WebElement getAddCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_confirm");
    }

    public static WebElement getManageMyCards(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_manageCreditCard");
    }

    public static WebElement getDeleteCardicon(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_deleteCard");
    }

    public static WebElement getDeleteconfirmationoption(final AndroidDriver driver) {
        return driver.findElementById("android:id/button1");
    }

    // find "no" option from confirmation delete
    public static WebElement getNotDeleteconfirmationoption(final AndroidDriver driver) {
        return driver.findElementById("android:id/button2");
    }

    public static WebElement getSnackBar(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/snackbar_text");
    }

    //Get Recharge BTN once click on ADSL from side menu
    public static WebElement getADSLRechageBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/walletRechargeBtn");
    }

    public static WebElement getErrorMsgforwrongCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/alertSecondaryText");
    }

    public static WebElement getAddNewCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/addCreditCardBtn");
    }

    public static WebElement getCrCardnameField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_card_name");
    }

    public static WebElement getCrCardNumberField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_card_number");
    }

    public static WebElement getCrCardExpYearField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='YY']");
    }

    public static WebElement getCrCardExpMonthField(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='MM']");
    }

    public static WebElement getCrCardCVV(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_cvv");
    }

    public static WebElement getAddThisCrCardBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/btn_add");
    }

    //payment History
    public static WebElement getPaymentHistoryBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/walletPaymentHistoryBtn");
    }

    public static WebElement getTotalDate(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/dateTV");
    }

    public static WebElement getTrxDateTime(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/dateTimeTV");
    }

    public static WebElement getTrxADSLNo(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/recipientTV");
    }

    public static WebElement getTrxAmount(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/paidAmountTV");
    }

    public static WebElement getTrxID(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/trxIdTV");
    }


}
