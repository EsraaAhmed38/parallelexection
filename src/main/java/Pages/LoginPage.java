/*package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.android.AndroidDriver;


public class LoginPage

{

	public static WebElement getloginNumEn(final AndroidDriver driver) {
		//return driver.findElementById("com.emeint.android.myservices:id/etOnboarding");
		return driver.findElement(By.xpath("//android.widget.EditText[@text='Type a mobile number']"));
	}
			
	public static WebElement getPasswordEn(final AndroidDriver driver) {
		//return driver.findElement(By.id("com.emeint.android.myservices:id/etOnboarding"));
		return driver.findElementByXPath("//android.widget.EditText[@text='Password']");
	}
	
	public static WebElement getloginNumAr(final AndroidDriver driver) {
		//return driver.findElementById("com.emeint.android.myservices:id/etOnboarding");
		return driver.findElement(By.xpath("//android.widget.EditText[@text='ادخل رقم الموبيل']"));
	}
	
	public static WebElement getPasswordAr(final AndroidDriver driver) {
		//return driver.findElement(By.id("com.emeint.android.myservices:id/etOnboarding"));
		return driver.findElementByXPath("//android.widget.EditText[@text='الرقم السري']");
	}

	public static WebElement getLoginButton(final AndroidDriver driver) {
		return driver.findElement(By.id("com.emeint.android.myservices:id/btnLogin"));
	}
   
}*/

package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class LoginPage {

    public static WebElement getloginNumEn(final AndroidDriver driver) {
        // return driver.findElementById("com.emeint.android.myservices:id/etOnboarding");
        return driver.findElement(By.xpath("//android.widget.EditText[@text='Type a mobile number']"));
        // return driver.findElement(By.xpath("//android.widget.EditText[@text='01090969989']"));

    }

    public static WebElement getPasswordEn(final AndroidDriver driver) {
        //return driver.findElement(By.id("com.emeint.android.myservices:id/etOnboarding"));
        return driver.findElementByXPath("//android.widget.EditText[@text='Password']");
    }

    public static WebElement getloginNumAr(final AndroidDriver driver) {
        //return driver.findElementById("com.emeint.android.myservices:id/etOnboarding");
        return driver.findElement(By.xpath("//android.widget.EditText[@text='ادخل رقم الموبيل']"));
    }

    public static WebElement getPasswordAr(final AndroidDriver driver) {
        //return driver.findElement(By.id("com.emeint.android.myservices:id/etOnboarding"));
        return driver.findElementByXPath("//android.widget.EditText[@text='الرقم السري']");
    }

    public static WebElement getLoginButton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnLogin"));
    }

}

