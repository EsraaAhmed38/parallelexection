package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class BillLimit {

    // Existence of bill limit
    public static WebElement getSetBillLimitTitle(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tvTitleBillLimit"));
    }

    // enable the toggle
    public static WebElement getEnabletheTogle(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/scBillLimit"));
    }

    // Enter an invalid limit and notice that the button is dimmed
    public static WebElement getSetBillLimit(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='Enter your limit']"));
    }

    // Enter an invalid limit and notice that the button is dimmed
    public static WebElement getSetBillLimitAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.EditText[@text='ادخل القيمة']"));
        //return driver.findElement(By.id("com.emeint.android.myservices:id/etBillLimit"));

    }

    public static WebElement getClickSetLimitBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnSetLimit"));

    }

    public static WebElement getClickDoneBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/doneBtn"));
    }

    public static WebElement checkExistenseOfSetBillLimit(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/scBillLimit"));
    }

    // disable the toggle
    public static WebElement getDisabletheTogle(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/scBillLimit"));
    }

    // click on remove limit
    public static WebElement getRemoveLimit(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/actionOneBtn"));
    }

    // click on done
    public static WebElement getClickDoneButton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/doneBtn"));
    }
}
