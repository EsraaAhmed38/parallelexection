package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

public class Nudge_ArabicPage {

    public static WebElement getMIButton(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='الموبايل إنترنت']");
    }

    public static WebElement getBackToSideMenu(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/toolbar_menu_icon");
    }

    public static WebElement getBackToHome(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/popular_icon");
    }

    public static WebElement getNudgeEntry(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/arcBg");
    }

    public static WebElement getOfferDes(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/subTitleTv");
    }

    public static WebElement getOfferTitle(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/titleTv");
    }

    public static WebElement getBackHome(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/toolbar_backIcon");
    }

    public static WebElement getBalance(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/points");
    }

    public static WebElement getQuota(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/data");
        //	com.emeint.android.myservices:id/overage_data  /////in case 0 quota
        //	com.emeint.android.myservices:id/data   // in case have quota
    }


    public static WebElement getRedeemBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/yesBtn");
    }

    public static WebElement getConfirmBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/yesBtn");
    }


    public static WebElement backtoHome(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/iv_side_logo");
    }


    public static WebElement getCloseBTN(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/noBtn");
    }

    public static WebElement LogOut(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/menu_item_icon");
    }

    public static WebElement getOKForLogOut(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/vb_operation");
    }

    public static WebElement getsettingandsupport(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Settings & Support']");
    }

    public static WebElement getsetting(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Settings']");
    }

    public static WebElement getlangArabic(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/spinner_item_name");

    }

    public static WebElement getchooselang(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='عربي']");
    }


}

