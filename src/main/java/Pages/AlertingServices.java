package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AlertingServices {

//	public static WebElement getSideMenu(final AndroidDriver driver) {
//		return driver.findElementById("com.emeint.android.myservices:id/iv_side_menu");
//	}

    public static WebElement getCard(final AndroidDriver driver, String category) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='" + category + "']"));
    }

    public static WebElement GetExploreTab(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Explore']");
    }

    public static WebElement getServicesDropdownList(final AndroidDriver driver) {

        return driver.findElementById("com.emeint.android.myservices:id/sp_services_category");
    }

    public static WebElement getSubscribeButton(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.Button[@text='Subscribe']");
    }

    public static WebElement getSubscribeConfrimationButton(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.Button[@index='0']");
    }


    public static WebElement GetEntertainment_Image(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/ivBackground5");
    }

    public static WebElement getAlerting_services(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Alerting services']");

    }

    public static WebElement getStarted_button(final AndroidDriver driver) {
        return driver.findElementByClassName("android.widget.Button");
    }

    public static WebElement GetMyServicesTab(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='My services']");

    }

    public static WebElement ServiceTitle(final AndroidDriver driver, String Category) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='" + Category + "']"));
    }

    public static WebElement getUnsubscribeConfrimationButton(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.Button[@text='Unsubscribe']");
    }

    public static WebElement getBackToMyServicesButton(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.Button[@text='Back to my services']");
    }

    public static WebElement getSuccessTitle(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_title_text");
    }

    public static WebElement getSearchField(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/et_search");
    }

    public static WebElement getSearchResult(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tv_title");
    }

    public static WebElement getExploreOtherservicesButton(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.Button[@text='Explore other services']");
    }

    public static WebElement getUnsubscribeButton(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.Button[@text='Unsubscribe']");

    }

}

	/*//
	
	
	
	public static WebElement getServices(final AndroidDriver driver) {

		return driver.findElementById("com.emeint.android.myservices:id/header");
	}

	

	public static WebElement GetEntertainmentTitle(final AndroidDriver driver) {
		return driver.findElementById("com.emeint.android.myservices:id/tvSectionTitle");
	}

	public static WebElement GetEntertainment_Image(final AndroidDriver driver) {
		return driver.findElementById("com.emeint.android.myservices:id/ivBackground5");
	}

	public static String EntertainmentToolbar_Title(final AndroidDriver driver) {
		String Title = driver.findElementById("com.emeint.android.myservices:id/toolbar_title").getText();
		return Title;
	}

	public static WebElement getAltertingServices_card(final AndroidDriver driver) {
		return driver.findElementByClassName("android.widget.LinearLayout");
	}

	

	public static WebElement AlertingServicesPage_Title(final AndroidDriver driver) {
		return driver.findElementById("com.emeint.android.myservices:id/toolbar_title");
	}

	

	

	

	

	

	public static WebElement getEntertainmentServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Entertainment']");
	}

	public static WebElement getReligiousServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Religious']");
	}

	public static WebElement getTipsServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Tips']");
	}

	public static WebElement getSportsServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Sports']");
	}

	public static WebElement getWomenServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Women']");
	}

	public static WebElement getHealthServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Health']");
	}

	public static WebElement getEconomicServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Economic']");
	}

	public static WebElement getCookingServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Cooking']");
	}

	public static WebElement getCareerServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Career']");
	}

	public static WebElement getRelationshipsServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Relationships']");
	}

	public static WebElement getChristianityServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Christianity']");
	}

	public static WebElement getHoroscopesServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Horoscopes']");
	}

	public static WebElement getChildCareServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Child Care']");
	}

	public static WebElement getWeatherServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Weather']");
	}

	public static WebElement getCarsServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Cars']");
	}

	public static WebElement getTrafficServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Traffic']");
	}

	public static WebElement getGamesServices(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Games']");
	}

	

	
	

	

	

	public static WebElement getSuccessImage(final AndroidDriver driver) {
		return driver.findElementById("com.emeint.android.myservices:id/alertImgView");
	}

	

	
	
	public static WebElement getAnswerWin2LETitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Answer & Win 2LE']");
	}
	public static WebElement getDeenWHayahTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Deen W Hayah']");
	}
	public static WebElement getFarmingTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Farming']");
	}
	public static WebElement getSalahWorldTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Salah World']");
	}
	public static WebElement getBeautyTipsTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Beauty Tips']");
	}
	public static WebElement getFoodBenefitsTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Food Benefits']");
	}
	public static WebElement getAwfarlakTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Awfarlak']");
	}
	public static WebElement getCbcSofraTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Cbc Sofra']");
	}
	public static WebElement getGovernmentJobsTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Government Jobs']");
	}
	public static WebElement getRelationshipServiceTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Tips For Sexual Health']");
	}
	public static WebElement getChristianityTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Contemplation In God’S Graces']");
	}
	public static WebElement getHoroscopesTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Horoscopes']");
	}
	public static WebElement getSchoolTipsTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='School Tips']");
	}
	public static WebElement getWeatherTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Hay2Et El Arsad']");
	}
	public static WebElement getCarsTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Cars Info']");
	}
	public static WebElement getTrafficTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='Salik']");
	}
	public static WebElement getGamesTitle(final AndroidDriver driver) {
		return driver.findElementByXPath("//android.widget.TextView[@text='How it is made']");
	}
	
	*/
	

