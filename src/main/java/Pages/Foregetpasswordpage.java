package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Foregetpasswordpage {
    //Pages.LoginPage();


    public static WebElement getForgetit(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnAction"));
    }

    public static WebElement gettemppassword(final AndroidDriver driver) {
        //return driver.findElement(By.xpath("//android.widget.EditText[@text='Enter temp password']"));
        return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.EditText"));
    }

    public static WebElement getenternewpassword(final AndroidDriver driver) {
        //	return driver.findElement(By.xpath("//android.widget.EditText[@text='Enter new password (min 8 characters)']"));
        return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.EditText"));
    }

    public static WebElement getenterretypenewpassword(final AndroidDriver driver) {
        //	return driver.findElement(By.xpath("//android.widget.EditText[@text='Retype new password (min 8 characters)']"));
        return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.view.ViewGroup/android.widget.EditText"));
    }

    public static WebElement getchangebutton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnChange"));
    }

    public static WebElement getvalidationmsg(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tvError"));
    }

    public static WebElement getweekPassOverlay(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tv_title_text"));
    }

    public static WebElement getOkweekPassOverlay(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/vb_operation"));
    }

    public static WebElement getweekPassScript(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tv_secondary_text"));
    }

}
