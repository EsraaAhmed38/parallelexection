package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HomePageRevamppage {
    public static WebElement getbalanceentrypoint(final AndroidDriver driver) {
        //return driver.findElement(By.id("com.emeint.android.myservices:id/pointsLayoutBg"));
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Balance']"));

    }

    public static WebElement getrechagreheader(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tv_balance_header"));
    }

    public static WebElement getBillCard(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/backgroundImage"));

        //return driver.findElement(By.xpath("//android.widget.TextView[@text='View your bill']"));
    }

    public static WebElement getBillAssert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My Bill']"));
    }

    public static WebElement getbackbutton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/toolbar_backIcon"));
    }

    public static WebElement getUsedminuteclick(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Minutes']"));
    }

    public static WebElement getUsedSMSclick(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='SMS']"));
    }

    public static WebElement getUsedminuteclickAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='دقائق']"));
    }

    public static WebElement getinternet(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Internet']"));
    }

    public static WebElement getinternetAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='إنترنت']"));
    }

    public static WebElement getinternetbutton(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnAction"));
    }

    public static WebElement getbalancevalue(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tvTypeValue"));
    }

    public static WebElement getinternetassert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Mobile Internet Management']"));
    }

    public static WebElement getinternetassertAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='إدارة الموبايل انترنت']"));
    }

    public static WebElement getFamilyTab(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Family']"));
    }

    public static WebElement getFamilyinternet(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Internet']"));
    }

    public static WebElement getFamilyinternetAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='ميجابيتس العيلة']"));
    }

    public static WebElement getFamilyminute(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Family minutes']"));
    }

    public static WebElement getFamilyminuteAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='دقائق العيلة']"));
    }

    public static WebElement getFamilySmS(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Family SMS']"));
    }

    public static WebElement getFamilySmSAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='رسائل العيلة']"));
    }

    public static WebElement getroaming(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Roaming']"));
    }

    public static WebElement getSbuscriberoaming(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='Subscribe']"));
    }

    public static WebElement getroamingassert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Roaming']"));
    }

    public static WebElement getFlex(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Flex']"));
    }

    public static WebElement getFlexManage(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/getMoreAddonsBtn"));
    }

    public static WebElement getflexassert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Flex Management']"));
    }

    public static WebElement getroamingAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='التجوال']"));
    }

    public static WebElement getSbuscriberoamingAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='اشترك الآن']"));
    }

    public static WebElement getroamingassertAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='باقات التجوال']"));
    }

    public static WebElement getminuteassert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Minutes']"));
    }

    public static WebElement getFamilyinternetassert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Family internet']"));
    }

    public static WebElement getFamilyminuteassert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Family minute']"));
    }

    public static WebElement getFamilySMSassert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Family SMS']"));
    }

    public static WebElement getRedPoint(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My points']"));
    }

    public static WebElement getRedPointassert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Your available points']"));
    }

    public static WebElement getRedPointAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='RED نقاط']"));
    }

    public static WebElement getRedPointassertAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='نقاطك الحالية']"));
    }

    public static WebElement getDiscoverMoreTitle(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Stay home, Stay connected ']"));
    }

    public static WebElement getVfCashIconFromHome(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Vodafone Cash']"));
    }

    public static WebElement getDslIconFromHome(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Vodafone DSL']"));
    }

    public static WebElement getSuperFlex(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Super Flex']"));
    }

    public static WebElement getSuperFlexAssert(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btn_subscribe"));
    }

    public static WebElement get365Offer(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='365 Offers']"));
    }

    public static WebElement get365OfferAssert(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/playagain_btn"));
    }

    public static WebElement getFlexExtra(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Flex Extra']"));
    }

    public static WebElement getFlexExtraAssert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Flex Extras']"));
    }

    public static WebElement getFlexTransfer(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Flex Transfer']"));
    }

    public static WebElement getFlexTransferAssert(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/flex_ranges_sp"));
    }

    public static WebElement getUSBIconFromHome(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='USB']"));
    }

    public static WebElement getMobileInternetIconFromHome(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Mobile internet']"));
    }

    public static WebElement getSetBillLimitFromHome(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Set Bill Limit']"));
    }

    public static WebElement getRedFamilyFromHome(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Red Family']"));
    }

    public static WebElement getStayInControlSection(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Stay in control']"));
    }

    public static WebElement getOtherServicesSection(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Other services']"));
    }

    public static WebElement getMyFlexSection(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My Flex']"));
    }

    public static WebElement getPayBillForOthers(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Pay bill for others']"));
    }

    public static WebElement getWhitelist(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Whitelist']"));

    }

    public static WebElement getFlexCard(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Flex']"));

    }

    public static WebElement getRechargeForOthers(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge for others']"));

    }

    public static WebElement getBalanceTransfer(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Balance Transfer']"));

    }

    public static WebElement getShowMore(final AndroidDriver driver) {
        return driver.findElement(By.id("//android.widget.TextView[@text='Show more']"));

    }

    public static WebElement getBlackList(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Blacklist']"));

    }

    public static WebElement getMCK(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='MCK']"));

    }

    public static WebElement getStoreLocator(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Store Locator']"));

    }

    public static WebElement getBalanceDetails(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Balance  Details']"));

    }

    public static WebElement getSallefnyShokran(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Salefny Shokran']"));

    }

    public static WebElement get3al7saby(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='3ala 7esaby']"));

    }

    public static WebElement getKallemnyShokran(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Kalemny Shokran']"));

    }

    public static WebElement getKallemnyShokranAssert(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Shokran services']"));

    }

    public static WebElement getShowLess(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Show Less']"));

    }

    public static WebElement getSalefnyBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/sallefny_shokran_btn"));

    }

    public static WebElement getEntertainment(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Stay Home, Stay Entertained']"));

    }

    public static WebElement getStorelocationBtn(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/store_locator_use_my_loc_btn"));

    }

    public static WebElement get3ala7esabyTitle(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='3ala 7esaby']"));

    }

    public static WebElement Entertainmentsection(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/ivBackground5"));

    }

    public static WebElement getMCKPage(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Missed Call Keeper']"));

    }

    public static WebElement getBlackListPage(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Blacklist Service']"));

    }

    public static WebElement getWhitelistPage(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Whitelist Service']"));

    }

    public static WebElement getBalanceTransferPage(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Balance Transfer']"));

    }

    public static WebElement getRechargeForOthersPage(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Recharge for others']"));

    }

    public static WebElement getBalanceDetailsPage(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Balance Details']"));

    }

    public static WebElement getPayForOthersPage(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Pay for others']"));

    }

}
