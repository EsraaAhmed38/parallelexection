package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginSeamlesPage {

    public static WebElement getloginSeamless(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnSeamless"));


    }

    public static WebElement getinvaildnumber(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/etOnboarding"));
    }

    public static WebElement getproceed(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/btnLogin"));
    }

    public static WebElement getloginseamlessassert(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/iv_side_logo"));
    }

    public static WebElement getloginUSBassert(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tvError"));
    }

    public static WebElement getloginregisternumber(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tvOnboardingVerificationHeader"));
    }

    public static WebElement getRedlogin(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='RED points']"));
    }

    public static WebElement getRedloginAR(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='RED نقاط']"));
    }


    public static By getloginseamlesassert(final AndroidDriver driver) {
        return (By.id("com.emeint.android.myservices:id/iv_side_logo"));
    }

    public static By getinavildnumberassert(final AndroidDriver driver) {
        return By.id("com.emeint.android.myservices:id/tvError");
    }

    public static By getUSBNumberassert(final AndroidDriver driver) {
        return By.id("com.emeint.android.myservices:id/tvError");
    }

    public static By getloginwithnumbernotregistered(final AndroidDriver driver) {
        return By.id("com.emeint.android.myservices:id/tvOnboardingVerificationHeader");
    }

    public static By getRednumber(final AndroidDriver driver) {
        return By.xpath("//android.widget.TextView[@text='RED points']");
    }

    public static By getRednumberAR(final AndroidDriver driver) {
        return By.xpath("//android.widget.TextView[@text='RED نقاط']");
    }

}