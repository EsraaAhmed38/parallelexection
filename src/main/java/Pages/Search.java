package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class Search {

    public static WebElement getSearch(final AndroidDriver driver) {

        return driver.findElementById("com.emeint.android.myservices:id/ivSearchIcon");
    }

    public static WebElement getSearchEdit(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/etSearch");
    }
	
	/*public static WebElement getSearchEditAr(final AndroidDriver driver) {
		return driver.findElement(By.xpath("//android.widget.EditText[@text='ابحث عن']"));
	}*/

    public static WebElement getSearchResult(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/tvSearchResultLabel");
    }

    public static WebElement getSearchClose(final AndroidDriver driver) {
        return driver.findElementById("com.emeint.android.myservices:id/ivClose");
    }

    public static WebElement get365Search(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='365 Offers']"));
    }

    public static WebElement getMyplanSearch(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My plan']"));
    }

    public static WebElement getBalancedetailsSearch(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Balance details ']"));
    }

    public static WebElement getInternetOffersSearch(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='Mobile internet offer']"));
    }

    public static WebElement getDSLSearch(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='DSL  ']"));
    }

    public static WebElement getRedFamilySearch(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='My Red Family']"));
    }

    public static WebElement getNoSearchResult(final AndroidDriver driver) {
        return driver.findElement(By.id("com.emeint.android.myservices:id/tvNoResult"));
    }

}
