package Pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class LogoutPage {

    public static WebElement getLogoutButton(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='Logout']");
    }

    public static WebElement getOKBtn(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='OK']"));
    }

    public static WebElement getLogoutButtonAr(final AndroidDriver driver) {
        return driver.findElementByXPath("//android.widget.TextView[@text='الخروج']");
    }

    public static WebElement getOKBtnAr(final AndroidDriver driver) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='نعم']"));
    }

}
